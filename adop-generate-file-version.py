"""
Usage:
    python adop-generate-file-version.py
    cat file_version_info.txt
"""

import adop

ProductName = adop.__name__
ProductVersion = adop.__version__

file_version_info = f"""VSVersionInfo(
  ffi=FixedFileInfo(),
  kids=[
    StringFileInfo([
      StringTable(
        '000004b0', [
          StringStruct('ProductName', '{ProductName}'),
          StringStruct('ProductVersion', '{ProductVersion}')
        ])
    ]),
    VarFileInfo([VarStruct('Translation', [0, 1200])])
  ]
)"""

print("write to file_version_info.txt")
with open("file_version_info.txt", "w") as f:
    print(file_version_info, file=f)
