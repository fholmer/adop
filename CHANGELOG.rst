Changelog
=========

Version 0.0.9
-------------

*2024.03.16*

**Bug fixes**

- The parsed return value from HG_PATS can now be both upper and
  lower case.


Version 0.0.8
-------------

*2024.02.24*

**Bug fixes**

- Parsed return value from HG_PATS must be lowercase
- ``zip install``: command must accept both ``application/zip`` and
  ``application/x-zip-compressed`` content type

Version 0.0.7
-------------

*2023.07.21*

**Enhancements**

- Added api-client
- Install sequence yields a return-code and return-data.

Version 0.0.6
-------------

*2023.04.23*

**Enhancements**

- ``zip install``: positional argument is optional
- ``zip upload``: positional argument is optional
- ``zip import``: positional argument is optional
- Add Windows service in setup file
- Added ``--force-upload`` and ``--force-deploy`` option to zip upload
- Webhook: support Gitea

Version 0.0.5
-------------

*2022.12.31*

**Enhancements**

- Print a helpful message if server modules are not installed
- Added command ``config add-remote``
- Added command ``config add-install``
- Added command ``config add-autofetch``

**Incompatible API changes**

- Moved server requirements into extra option.
- Rename and split ``auto_sequences.py``
- Changed defaults: deploy_path, install_path, cache_path

Version 0.0.4
-------------

*2022.12.04*

**Enhancements**

- Added command ``zip import``
- Added command ``config open``
- Added multiple ``--remote`` option to zip upload/install/import
- Added module ``adop.tool.hg``. Useful when configuring hooks in hg.

**Bug fixes**

- Fix: list of multiple autofetch sources was broken
- Fix: pre-deploy hook did not abort deployment if exit code was >0

Version 0.0.3
-------------

*2022.10.30*

**Enhancements**

- ``zip upload``: Check if file is already uploaded.
- Webhooks: support Gitlab and Github secret tokens.
- Hooks: support for ``pre-deploy`` and ``post-deploy``
- Added ``config import``

Version 0.0.2
-------------

*2022.10.22*

**Enhancements**

- Late import of api_serve.
- Added command: ``config generate-token``.
- Added hook for reading keyring if installed.
- Added endpoint: ``api/v1/list/zip``.
- Added ``--merge`` option to ``config init`` command.
- Added support for ``Zip-Root`` header.
- Change config-format for auto-fetch function.

**Bug fixes**

- Unpacking routine will remove directories if missing in zip-file.
- Fixed logging in auto-fetch routine.

Version 0.0.1
-------------

*2022.10.02*

**Bug fixes**

- Fixed exit-codes on ``KeyboardInterrupt`` and ``CommandFail``.

Version 0.0.1b6
---------------

*2022.09.25*

- New exception class: CacheNotFound
- Fix command: config init
- Fix state: upload command should not update deploy state.
- Replace ``auth_token`` with ``write_token`` and ``read_token``.
- Fix error message in zip upload command.
- Added hook for reading environment variables
- Added ``--install`` option to ``zip install`` and ``zip upload``
- Added ``--deploy`` option to ``zip upload``
- Added support for tags.

Version 0.0.1b5
---------------

*2022.09.21*

- New sub-commands: adop config
- Changed default config for [client] section
- Removed config [auth] and [paths]
- Added limited hooks support
- New command: adop zip upload

Version 0.0.1b4
---------------

*2022.09.11*

- New command: adop zip install

Version 0.0.1b3
---------------

*2022.08.19*

- Added option for version
- Updated docs and docstrings

Version 0.0.1b2
---------------

*2022.08.15*

- Moved the ``Root`` argument from HTTP headers to URL endpoint
- Updated docs

Version 0.0.1b1
---------------

*2022.08.12*

- Added docs.

Version 0.0.1a5
---------------

*2022.08.06*

- Added app version to windows build
- Added exception class

Version 0.0.1a4
---------------

*2022.08.04*

- Changed return codes and error messages
- Changed default config file path
- ``auto_fetch``: Fixed unpacking routine
- ``handle_zip``: always complete unpacking sequence.
- Added logging module
- Removed ``auto_fetch`` examples from default config files.
- Added ``--cwd`` argument


Version 0.0.1a3
---------------

*2022.07.28*

- Improved ``verify_root`` function
- Added adop-setup


Version 0.0.1a2
---------------

*2022.07.17*

- Added tests, scripts, code comments and more

Version 0.0.1a1
---------------

*2022.07.13*

- Initial version