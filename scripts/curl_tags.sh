#!/usr/bin/env sh
endpoint=$ADOP_URL/tags/
token=$ADOP_TOKEN

curl \
    -H "Token: $token" \
    $endpoint$1
