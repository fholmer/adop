#!/usr/bin/env sh
endpoint=$ADOP_URL/trigger/fetch
token=$ADOP_TOKEN

somedata="{\"key\":\"payload\"}"
signature=$(printf "$somedata" | openssl sha256 -hmac $token)
signature=${signature#*= }
curl \
    -H "x-hub-signature-256: sha256=$signature" \
    -d "$somedata" \
    $endpoint/gh
