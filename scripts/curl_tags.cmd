@echo off
set endpoint=%ADOP_URL%/state
set token=%ADOP_TOKEN%

curl ^
    -H "Token: %token%" ^
    %endpoint%%1%
