#!/usr/bin/env sh
endpoint=$ADOP_URL/trigger/fetch
token=$ADOP_TOKEN

curl \
    -H "Token: $token" \
    $endpoint
