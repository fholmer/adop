#!/usr/bin/env sh
endpoint=$ADOP_URL/deploy/zip
token=$ADOP_TOKEN
sum=$(sha256sum $1)

curl \
    -H "Zip-Sha256: ${sum%% *}" \
    -H "Token: $token" \
    $endpoint/$(basename "${1%.*}")
