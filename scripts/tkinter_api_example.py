# http://tkdocs.com/tutorial/morewidgets.html#text
# https://docs.python.org/3/library/tkinter.html

import glob
import json
import ssl
from concurrent.futures import ThreadPoolExecutor
from queue import Queue
from tkinter import HORIZONTAL, DoubleVar, StringVar, Tk, ttk
from urllib import request

endpoints = [
    "http://127.0.0.1:8000/api/v1",
    "https://adop-proxy.local/api/v1",
]

zip_files = []


class App:
    def __init__(self, root, workers) -> None:
        self.root: Tk = root
        self.workers: ThreadPoolExecutor = workers
        self.url: StringVar = StringVar(root, endpoints[0])
        self.token: StringVar = StringVar(root, "NO")
        self.progress: ttk.Progressbar = None
        self.upload_progress = DoubleVar(root, 0.0)
        self.zip_files: list = zip_files + glob.glob("work/*.zip")
        self.zip_file: StringVar = StringVar(
            root, self.zip_files[0] if self.zip_files else ""
        )
        self.zip_root: StringVar = StringVar(root, "")
        self.results: dict[str, StringVar] = {}
        self.setup_ttk()

    def setup_ttk(self):
        frm = ttk.Frame(self.root, padding=10)
        frm.grid(sticky="news")

        self.root.columnconfigure(0, weight=1)
        frm.columnconfigure(2, weight=1)

        self.root.rowconfigure(0, weight=1)

        row = 0
        ttk.Label(frm, text="API URL:").grid(column=0, row=row)
        ttk.Combobox(frm, values=endpoints, textvariable=self.url).grid(
            column=1, columnspan=2, row=row, sticky="ew"
        )

        row += 1
        ttk.Label(frm, text="Token:").grid(column=0, row=row)
        ttk.Combobox(
            frm, values=[self.token.get()], textvariable=self.token, width=64
        ).grid(column=1, columnspan=2, row=row, sticky="ew")

        row += 1
        ttk.Label(frm, text="Zip file:").grid(column=0, row=row)
        ttk.Combobox(frm, values=self.zip_files, textvariable=self.zip_file).grid(
            column=1, columnspan=2, row=row, sticky="ew"
        )

        row += 1
        ttk.Label(frm, text="Zip root:").grid(column=0, row=row)
        ttk.Combobox(frm, values=[], textvariable=self.zip_root).grid(
            column=1, columnspan=2, row=row, sticky="ew"
        )

        row += 1
        ttk.Label(frm, text="Progress:").grid(column=0, row=row)
        ttk.Button(
            frm,
            text="Clear",
            command=self.clear_ttk,
        ).grid(column=1, row=row, sticky="w")
        self.progress = ttk.Progressbar(
            frm, orient=HORIZONTAL, mode="indeterminate", maximum=25
        )
        self.progress.grid(column=2, row=row, sticky="ew")

        row += 1
        self.results["upload_and_deploy"] = StringVar(self.root, "Result ...")
        ttk.Label(frm, text="Upload and deploy\nwith streaming result").grid(
            column=0, row=row
        )
        ttk.Button(
            frm,
            text="POST /api/v1/deploy/zip",
            command=self.run_post_deploy_zip_streaming,
        ).grid(column=1, row=row, sticky="w")
        ttk.Label(frm, textvariable=self.results["upload_and_deploy"]).grid(
            column=2, row=row, sticky="ew"
        )

        row += 1
        ttk.Label(frm, text="Upload and deploy\nwith progressbar").grid(
            column=0, row=row
        )
        ttk.Button(
            frm,
            text="POST /api/v1/deploy/zip",
            command=self.run_post_deploy_zip_progress,
        ).grid(column=1, row=row, sticky="w")
        ttk.Progressbar(
            frm, variable=self.upload_progress, orient=HORIZONTAL, mode="determinate"
        ).grid(column=2, row=row, sticky="ew")

        row += 1
        self.results["test"] = StringVar(self.root, "Result ...")
        ttk.Label(frm, text="Test api").grid(column=0, row=row)
        ttk.Button(frm, text="GET /api/v1/test", command=self.run_api_test).grid(
            column=1, row=row, sticky="w"
        )
        ttk.Label(frm, textvariable=self.results["test"]).grid(
            column=2, row=row, sticky="ew"
        )

        row += 1
        self.results["state"] = StringVar(self.root, "Result ...")
        ttk.Label(frm, text="Check state").grid(column=0, row=row)
        ttk.Button(frm, text="GET /api/v1/state", command=self.run_api_state).grid(
            column=1, row=row, sticky="w"
        )
        ttk.Label(frm, textvariable=self.results["state"]).grid(
            column=2, row=row, sticky="ew"
        )

        row += 1
        self.results["progress"] = StringVar(self.root, "Result ...")
        ttk.Label(frm, text="Check progress").grid(column=0, row=row)
        ttk.Button(
            frm, text="GET /api/v1/progress", command=self.run_api_progress
        ).grid(column=1, row=row, sticky="w")
        ttk.Label(frm, textvariable=self.results["progress"]).grid(
            column=2, row=row, sticky="ew"
        )

        row += 1
        frm.rowconfigure(row, weight=1)
        row += 1

        ttk.Label(frm, text="Quit").grid(column=0, row=row)
        ttk.Button(frm, text="Quit", command=self.root.destroy).grid(
            column=1, row=row, sticky="w"
        )


    def clear_ttk(self):
        for res in self.results.values():
            res.set("Result ...")
        self.progress.stop()
        self.upload_progress.set(0)

    def run_generator(self, iterable=None, first=True):
        if first:
            self.progress.start()
        try:
            next(iterable)
            self.root.after(10, self.run_generator, iterable, False)
        except StopIteration:
            self.progress.stop()

    def run_post_deploy_zip_streaming(self):
        def post(out: Queue):
            out.put("Posting data ...\n")
            for line in self.rest_post("deploy/zip"):
                out.put(line)

        def gen():
            buffer = ""
            out = Queue()
            future = self.workers.submit(post, out)
            while not future.done() or not out.empty():
                yield
                if not out.empty():
                    line = out.get()
                    buffer += line
                    self.results["upload_and_deploy"].set(buffer)
                    yield

        self.run_generator(gen())

    def run_post_deploy_zip_progress(self):
        def post():
            for _ in self.rest_post("deploy/zip"):
                pass

        def gen():
            self.upload_progress.set(1)
            future = self.workers.submit(post)
            while not future.done():
                yield
                zip_root = self.zip_root.get()
                progress_dict = json.loads(self.rest_get("progress"))
                if zip_root in progress_dict:
                    if "progress" in progress_dict[zip_root]:
                        self.upload_progress.set(progress_dict[zip_root]["progress"])
            self.upload_progress.set(100)

        self.run_generator(gen())

    def run_api_test(self):
        self.results["test"].set(self.rest_get("test"))

    def run_api_state(self):
        data = json.loads(self.rest_get("state"))
        self.results["state"].set(json.dumps(data, indent=2))

    def run_api_progress(self):
        data = json.loads(self.rest_get("progress"))
        for d in data.values():
            if "log" in d:
                del d["log"]
        self.results["progress"].set(json.dumps(data, indent=2))

    def rest_get(self, endpoint: str, headers: dict = {}):
        headers.update(Token=self.token.get())
        req = request.Request(url=f"{self.url.get()}/{endpoint}", headers=headers)
        try:
            context = ssl.create_default_context()
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE
            return request.urlopen(req, context=context).read()
        except Exception as err:
            return f"{err}"

    def rest_post(self, endpoint: str, headers: dict = {}):
        headers.update(Token=self.token.get())
        data = open(self.zip_file.get(), "rb").read()
        req = request.Request(
            url=f"{self.url.get()}/{endpoint}/{self.zip_root.get()}",
            data=data,
            headers=headers,
        )
        try:
            context = ssl.create_default_context()
            context.check_hostname = False
            context.verify_mode = ssl.CERT_NONE

            res = request.urlopen(req, context=context)
            buffer = b" "
            while buffer:
                buffer = res.readline(100)
                yield buffer.decode("utf8", errors="ignore")
        except Exception as err:
            yield f"{err}"


def main():
    workers = ThreadPoolExecutor(max_workers=5)
    root = Tk()
    App(root, workers)
    root.mainloop()
    workers.shutdown()


if __name__ == "__main__":
    main()
