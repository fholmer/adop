#!/usr/bin/env sh
endpoint=$ADOP_URL/trigger/fetch
token=$ADOP_TOKEN

somedata="{\"key\":\"payload\"}"
signature=$(printf "$somedata" | openssl sha256 -hmac $token)
signature=${signature#*= }
curl \
    -H "HTTP_X_GITEA_SIGNATURE: $signature" \
    -d "$somedata" \
    $endpoint/gt
