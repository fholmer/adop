@echo off
set endpoint=%ADOP_URL%/deploy/zip
set token=%ADOP_TOKEN%

set sum=
for /f "skip=1" %%i in ('certutil -hashfile "%1" SHA256') do if not defined sum set sum=%%i

curl ^
    -H "Zip-Sha256: %sum%" ^
    -H "Token: %token%" ^
    %endpoint%/%~n1
