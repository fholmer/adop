#!/usr/bin/env sh
endpoint=$ADOP_URL/test
token=$ADOP_TOKEN

curl \
    -H "Token: $token" \
    $endpoint
