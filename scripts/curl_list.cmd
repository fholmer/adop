@echo off
set endpoint=%ADOP_URL%/list/zip
set token=%ADOP_TOKEN%

curl ^
    -H "Token: %token%" ^
    %endpoint%%1%
