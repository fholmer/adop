@echo off
set endpoint=%ADOP_URL%/test
set token=%ADOP_TOKEN%

curl ^
    -H "Token: %token%" ^
    %endpoint%
