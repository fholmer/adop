# http://tkdocs.com/tutorial/morewidgets.html#text
# https://docs.python.org/3/library/tkinter.html

import shlex
import signal
import subprocess
import sys
from concurrent.futures import ThreadPoolExecutor
from tkinter import END, Text, Tk, scrolledtext, ttk


class App:
    def __init__(self, root: Tk, workers: ThreadPoolExecutor) -> None:
        self.root: Tk = root
        self.workers: ThreadPoolExecutor = workers
        self.setup_ttk()
        self.proc: subprocess.Popen = None

    def setup_ttk(self) -> None:
        frm = ttk.Frame(self.root, padding=10)
        frm.grid(sticky="news")

        self.root.columnconfigure(0, weight=1)
        frm.columnconfigure(0, weight=1)

        self.root.rowconfigure(0, weight=1)
        frm.rowconfigure(3, weight=1)

        ttk.Label(frm, text="Arguments:").grid(row=0, columnspan=3, sticky="w", pady=10)
        self.config = Text(frm, height=1)
        self.config.insert(END, "serve-api --config ./work/adop.ini")
        self.config.grid(row=1, columnspan=3, sticky="ew")

        ttk.Label(frm, text="Log:").grid(row=2, columnspan=3, sticky="w", pady=10)
        self.log = scrolledtext.ScrolledText(frm)
        self.log.grid(row=3, columnspan=3, sticky="news")

        ttk.Button(frm, text="Start", command=self.start).grid(
            row=4, sticky="w", pady=10
        )
        ttk.Button(frm, text="Stop", command=self.stop).grid(
            column=1, row=4, sticky="w", pady=10
        )
        ttk.Button(frm, text="Quit", command=self.destroy).grid(
            column=2, row=4, sticky="w", pady=10
        )

    def run_generator(self, iterable=None) -> None:
        try:
            next(iterable)
            self.root.after(10, self.run_generator, iterable)
        except StopIteration:
            pass

    def stop(self):
        if self.proc and self.proc.poll() is None:
            if hasattr(signal, "CTRL_C_EVENT"):
                self.root.after(0, self.proc.send_signal, signal.CTRL_C_EVENT)
                self.root.after(0, print)
            else:
                self.root.after(0, self.proc.send_signal, signal.SIGINT)

    def destroy(self):
        self.stop()
        self.root.destroy()

    def log_append(self, text, end="\n"):
        self.log.insert(END, f"{text}{end}")
        self.log.see(END)

    def start(self):
        def gen():
            if self.proc and self.proc.poll() is None:
                self.log_append(f"** PROC (pid:{self.proc.pid}) is already running **")
                return
            self.log_append("** STARTING **")
            config = self.config.get("1.0", END).strip()

            if sys.executable.endswith("adop-cli.exe"):
                executable = [sys.executable]
            else:
                executable = [sys.executable, "-m", "adop"]

            self.proc = subprocess.Popen(
                executable + shlex.split(config),
                stderr=subprocess.PIPE,
                stdout=subprocess.PIPE,
            )
            stdout_future = self.workers.submit(self.proc.stdout.readline)
            stderr_future = self.workers.submit(self.proc.stderr.readline)

            while self.proc.poll() is None:

                try:
                    if stdout_future.done():
                        self.log_append(
                            f"{stdout_future.result().decode(errors='ignore')}", ""
                        )
                        stdout_future = self.workers.submit(self.proc.stdout.readline)
                    if stderr_future.done():
                        self.log_append(
                            f"{stderr_future.result().decode(errors='ignore')}", ""
                        )
                        stderr_future = self.workers.submit(self.proc.stderr.readline)
                except Exception as err:
                    self.log_append(f"** ERR {err} **")
                yield
            yield
            self.log_append("** STOPPED **")

        self.run_generator(gen())


def ignore(sig, frame):
    pass


def main():
    with ThreadPoolExecutor(max_workers=5) as workers:
        root = Tk()
        root.protocol("WM_DELETE_WINDOW", App(root, workers).destroy)
        signal.signal(signal.SIGINT, ignore)
        root.mainloop()


if __name__ == "__main__":
    main()
