@echo off
set endpoint=%ADOP_URL%/trigger/fetch
set token=%ADOP_TOKEN%

curl ^
    -H "Token: %token%" ^
    %endpoint%
