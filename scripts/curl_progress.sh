#!/usr/bin/env sh
endpoint=$ADOP_URL/progress
token=$ADOP_TOKEN

curl \
    -H "Token: $token" \
    $endpoint
