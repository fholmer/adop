@echo off
set endpoint=%ADOP_URL%/deploy/zip
set token=%ADOP_TOKEN%

curl ^
    -H "Content-Type: application/zip" ^
    -H "Token: %token%" ^
    --data-binary "@%1" ^
    %endpoint%/%~n1
