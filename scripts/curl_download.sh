#!/usr/bin/env sh
endpoint=$ADOP_URL/download/zip
token=$ADOP_TOKEN

if test -z $1
then
    curl \
        -H "Token: $token" \
        $endpoint/

elif test -z $2
then
    curl -f \
        -H "Token: $token" \
        $endpoint/$1 | sha256sum
else
    curl -f \
        -H "Token: $token" \
        -H "Zip-Sha256: $2" \
        $endpoint/$1 | sha256sum
fi
