#!/usr/bin/env sh
endpoint=$ADOP_URL/state
token=$ADOP_TOKEN

curl \
    -H "Token: $token" \
    $endpoint$1
