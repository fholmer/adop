import pathlib
import ssl
import sys
import datetime
from urllib import request

tag = datetime.date.today().isoformat()
token = "NO"
file = pathlib.Path(sys.argv[1])
endpoint = f"http://127.0.0.1:8000/api/v1/deploy/zip/{file.stem}"

req = request.Request(url=endpoint, data=file.read_bytes(), headers={"Token": token, "Zip-Tag": tag})

context = ssl.create_default_context()
if "insecure" in sys.argv:
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE

res = request.urlopen(req, context=context)
buffer = b" "
while buffer:
    buffer = res.readline(100)
    print(buffer.decode(errors="ignore"), end="")
