#!/usr/bin/env sh
endpoint=$ADOP_URL/trigger/fetch
token=$ADOP_TOKEN

curl \
    -H "x-gitlab-token: $token" \
    -d "{\"test\":\"data\"}" \
    $endpoint/gl
