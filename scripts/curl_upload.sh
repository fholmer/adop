#!/usr/bin/env sh
endpoint=$ADOP_URL/upload/zip
token=$ADOP_TOKEN

curl \
    -H "Content-Type: application/zip" \
    -H "Token: $token" \
    -H "Zip-Tag: $(date -I)" \
    --data-binary "@$1" \
    $endpoint/$(basename "${1%.*}")
