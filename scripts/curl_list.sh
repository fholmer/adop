#!/usr/bin/env sh
endpoint=$ADOP_URL/list/zip
token=$ADOP_TOKEN

curl \
    -H "Token: $token" \
    $endpoint$1
