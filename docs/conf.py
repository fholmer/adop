# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import adop

project = "adop"
copyright = "2024, Frode Holmer"
author = "Frode Holmer"
release = adop.__version__
version = adop.__version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
]
intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
    "flask": ("https://flask.palletsprojects.com/en/2.1.x", None),
}

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

primary_domain = "py"
default_role = "py:obj"

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_logo = "_static/img/logo/logo.svg"
html_favicon = "_static/img/logo/favicon.svg"
html_theme = "furo"
html_static_path = ["_static"]
html_theme_options = {
    "sidebar_hide_name": True,
}

autodoc_typehints = "description"
autodoc_class_signature = "separated"

pygments_style = "friendly"
# Update api docs:
# sphinx-apidoc ../adop -ePo developer_guide/api
# Update logo
# python gen_logo.py > _static\img\logo\<insert-version>\logo.svg
