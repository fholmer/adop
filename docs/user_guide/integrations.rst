============
Integrations
============

.. _reverse_proxy:

Reverse-proxy
=============

A reverse proxy is a web-server that sits in front of the adop REST API and
forwards client request.

Here is a quick recipe to get started on Windows systems:

- Download Traefik Proxy from `GitHub <https://github.com/traefik/traefik/releases>`_

- Unpack the downloaded file into ``C:\Project\traefik``

- Create two files:

  .. code-block:: yaml
    :caption: File: traefik.yml
    :linenos:

    ## Static configuration

    global:
      checkNewVersion: false
      sendAnonymousUsage: false
    entryPoints:
      websecure:
        address: ":443"
        http:
          redirections:
            entryPoint:
              to: websecure
    providers:
      file:
        filename: dynamic.yml
    log:
      level: INFO


  .. code-block:: yaml
    :caption: File: dynamic.yml
    :linenos:

    # Dynamic Configuration

    tls:
      stores:
        default:
          defaultCertificate:
            #certFile: ./local.crt
            #keyFile: ./local_key

    http:
      routers:
        adop:
          entryPoints:
            - websecure
          rule: PathPrefix(`/api/`) || Host(`adop.local`)
          service: adop
          tls: {}

      services:
        adop:
          loadBalancer:
            servers:
              - url: "http://127.0.0.1:8000"


  
- Start Traefik:

  .. code-block:: doscon

    > traefik.exe

- Start a new *command prompt* and run adop

  .. code-block:: doscon

    > adop serve-api

- Start another *command prompt* and test with ``curl``

  .. code-block:: doscon

    > curl -k ^
      -H "Token: NO" ^
      -H "Host: adop.local" ^
      https://127.0.0.1/api/v1/test

  .. note:: 

    The ``Host:`` header value have to match the
    *traefik* ``adop.local`` rule;


.. _install_as_service:

Install as service
==================

To run adop as service you will need a third party tool. For linux based
systems it is recommended to use `systemd`_. For Windows systems you can
use a tool called `WinSW`_.

systemd
-------

Procedure:

- Install adop into ``/opt/adop``

  .. code-block:: console

    $ sudo python3 -m venv /opt/adop
    $ sudo /opt/adop/bin/pip install adop[server]

- Create file ``/etc/systemd/system/adop.service``

- Add content to the file:

  .. code-block:: ini
    :caption: File: /etc/systemd/system/adop.service
    :linenos:

    [Unit]
    Description=adop
    After=syslog.target network-online.target

    [Service]
    Type=simple
    User=1000
    Group=1000
    Environment=PYTHONUNBUFFERED=true

    WorkingDirectory=/opt/adop
    ExecStart=/opt/adop/bin/adop serve-api

    StandardOutput=syslog
    StandardError=syslog

    [Install]
    WantedBy=multi-user.target

- Install and start the service by typing following commands:

  .. code-block:: console

    $ sudo systemctl --system daemon-reload
    $ sudo systemctl enable adop.service
    $ sudo systemctl start adop.service


WinSW
-----

WinSW is included in the latest adop windows installer.

Procedure:

- Download the latest *Windows installer* from :ref:`binary_package`

- Install *adop* for *all users* at ``C:\Program Files\adop``.

- Select the *Install as service* option.

- ``adop.ini`` and ``adop-service.xml`` is located at ``C:\Program Files\adop``.


VCS Hooks
=========

Git
---

You can upload new or missing zip-files when doing a ``git push`` by adding a
``pre-push`` hook to git. If you want adop to upload to ``remote1`` when
pushing to ``origin``:

.. code-block:: bash

    # add the adop remote name into the git config file
    $ git config --add remote.origin.adop remote1
    # create a pre-push hook in git
    $ printf '#!/bin/sh
    > python -m adop zip upload requires.ini -r $(git config --get remote.$1.adop)
    > ' > .git/hooks/pre-push
    # make hook executable
    $ chmod a+x .git/hooks/pre-push

You should also update installed zip-files on git checkout:

.. code-block:: bash

    $ printf '#!/bin/sh
    > python -m adop zip install requires.ini -r remote1
    > ' > .git/hooks/post-checkout
    # make hook executable
    $ chmod a+x .git/hooks/post-checkout


Mercurial
---------

You can configure the ``pre-push`` hook to upload zip-files and the ``update``
hook to install zip-files for the local workspace. The following example
will add:

- adop zip upload to ``remote1`` when ``hg push`` or ``hg push default``
- adop zip upload to ``remote1`` when ``hg push server1``
- adop zip upload to ``remote2`` when ``hg push server2``
- adop zip install from ``remote1`` or ``remote2`` when ``hg update``

Just add/merge the following config into the ``.hg/hgrc`` file:

- Windows

  .. code-block:: ini

    [paths]
    default = https://hg.server1.local
    server1 = https://hg.server1.local
    server2 = https://hg.server2.local

    [adop:remotes]
    default = remote1
    server1 = remote1
    server2 = remote2

    [hooks]
    update.adop = py -um adop zip install requires.ini -r remote1 -r remote2
    pre-push.adop = for /f %i in ('py -um adop.tool.hg -r') do py -um adop zip upload requires.ini -r %i

- Linux

  .. code-block:: ini

    [paths]
    default = https://hg.server1.local
    server1 = https://hg.server1.local
    server2 = https://hg.server2.local

    [adop:remotes]
    default = remote1
    server1 = remote1
    server2 = remote2

    [hooks]
    update.adop = python -m adop zip install requires.ini -r remote1 -r remote2
    pre-push.adop = python -m adop zip upload requires.ini -r $(python3 -m adop.tool.hg -r)
