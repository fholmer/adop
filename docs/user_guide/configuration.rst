=============
Configuration
=============

The default config file is created by the ``adop config init`` command. It will
also be created automatically when the REST-API server is started.
You can start and stop the server by typing ``adop serve-api`` and then press
:kbd:`ctrl+c`.

For Linux systems the configuration is stored at ``~/.adop/adop.ini``. For 
Windows systems configuration is stored at ``%USERPROFILE%\.adop\adop.ini``.

adop have to be restarted after changes to the configuration, because the
configuration is only read at startup.

.. _default_configuration:

Server side defaults
====================

The default configuration will bind the server to localhost (127.0.0.1:8000):

.. code-block:: ini
    :linenos:

    [server]
    on = 1
    host = 127.0.0.1
    port = 8000

This is the recommended way of running adop. Always bind adop to 127.0.0.1 and
expose the server to the network behind a :ref:`HTTPS reverse-proxy <reverse_proxy>`.

The following options are also available, but it is not recommended to use
them in a production environment:

.. code-block:: ini
    :linenos:

    [server]
    debug = 0  # Enable server-side debugging
    ssl_on = 0  # Enable HTTPS
    ssl_certificate =  # path to cert
    ssl_certificate_key =  # path to priv-key

Authorization
-------------

The ``read_token`` will only allow access to viewing statuses
and downloading zip-files. ``write_token`` gives full access.

.. code-block:: ini
    :linenos:

    [server]
    write_token =
    read_token = ${write_token}

If the ``read_token`` option is missing, it will refere to ``write_token``.

Paths
-----

Zip-files will be cached to ``cache_root`` and deployed to ``deploy_root``
on server-side.

.. code-block:: ini
    :linenos:

    [server]
    deploy_root = ./lib/auto
    cache_root = ./lib/cache


Cache cleanup
-------------

Zip-files is deleted automatically from the cache. Only 5 zip-files for each
:term:`root` is kept on disk by default.

.. code-block:: ini
    :linenos:

    [auto_delete]
    on = 1
    keep_on_disk = 5

Glossary
========

.. glossary:: 

  **cache_root**
    The directory where all zip-files and metadata is stored.

  **deploy_root**
    The extraction directory for zip-files on :ref:`server_side`.

  **install_root**
    The extraction directory for zip-files on :ref:`client_side`.


.. _auto_fetch:

Auto fetch
==========

To activate the auto fetch background worker you have to merge the following
configuration into ``adop.ini``:

.. code-block:: ini
    :linenos:

    [autofetch]
    on = 1
    run_at_startup = 1
    interval = 7200
    sources = self_hosted_example

    [autofetch:self_hosted_example]
    root = simple
    check_url = https://example.local/api/v1/state/simple
    payload_url = https://example.local/api/v1/download/zip/simple
    headers = Token: NO, Host: distribute

``on``
  Enable the background worker.

``run_at_startup``
  Check all the configured ``sources`` at startup.

``interval``
  How many seconds to wait until next check.

``sources``
  A comma separated list of sources to process. The source refers to another
  section of the ini-file prefixed with ``autofetch:``.

The source section requires following keys:

``root``
  Name of the :term:`root` directory to deploy.

``zip_root``
  optional: Name of the :term:`root` directory of the zip-file. Can be used
  if the root name differs.

``check_url``
  Read the contents of this url. If the contents differs from
  last check, download and deploy the payload from ``payload_url``.

``payload_url``
  URL of the zip-file that should be deployed.

``headers``
  A comma separated list of HTTP headers required for the ``check_url``
  and ``payload_url`` request.


Examples
--------

.. code-block:: ini
    :linenos:

    [autofetch]
    on = 1
    run_at_startup = 1
    interval = 7200
    sources = github_example, gitlab_example, self_hosted_example

    [autofetch:github_example]
    root = gh_simple
    zip_root = simple-master
    check_url = https://api.github.com/repos/fholmer/simple/git/refs/heads/master
    payload_url = https://github.com/fholmer/simple/archive/refs/heads/master.zip

    [autofetch:gitlab_example]
    root = gl_simple
    zip_root = simple-master
    check_url = https://gitlab.com/api/v4/projects/fholmer%%2Fsimple/repository/branches/master
    payload_url = https://gitlab.com/fholmer/simple/-/archive/master/simple-master.zip
    headers = User-agent: Mozilla/5.0

    [autofetch:self_hosted_example]
    root = simple
    check_url = https://example.local/api/v1/state/simple
    payload_url = https://example.local/api/v1/download/zip/simple
    headers = Token: NO, Host: distribute


Client side defaults
====================

The ``adop zip install -r <REMOTE> -i <INSTALL>`` command options
refers to prefixed sections in the configfile.

-r <REMOTE>
  refers to the ``[remote:<REMOTE>]`` section.

-i <INSTALL>
  refers to the ``[install:<INSTALL>]`` section.


Remotes
-------

Options:

.. code-block:: ini
    :linenos:

    [remote:<REMOTE>]
    url =  # remote api url
    token =  # remote api token
    insecure =  # use to allow untrusted certificates

Pre-configured ``remote:`` sections:

.. code-block:: ini
    :linenos:

    [remote:local]
    url = http://${server:host}:${server:port}/api/v1
    token = ${server:write_token}
    insecure = 0

Install locations
-----------------

Options:

.. code-block:: ini
    :linenos:

    [install:<INSTALL>]
    install_root =  # install path
    cache_root =  # cache path

Pre-configured ``install:`` sections:

.. code-block:: ini
    :linenos:

    [install:homedir]
    install_root = ~/.adop-cache/auto
    cache_root = ~/.adop-cache/cache

    [install:basedir]
    install_root = ./lib/auto
    cache_root = ~/.adop-cache/cache

    [install:parentdir]
    install_root = ../lib/auto
    cache_root = ~/.adop-cache/cache

Client defaults
---------------

The ``adop zip install`` and ``adop zip upload`` command use the following
defaults:

.. code-block:: ini
    :linenos:

    [client]
    install = basedir
    remote = local

