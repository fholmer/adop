============
REST Clients
============

Examples of client implementations:

- `Curl (linux)`_
- `Curl (win)`_
- `Python`_


Curl (linux)
============

.. code-block:: sh
    :caption: Create file: deploy.sh
    :linenos:

    endpoint=http://127.0.0.1:8000/api/v1/deploy/zip
    token=NO

    curl \
        -H "Token: $token" \
        --data-binary "@$1" \
        $endpoint/$(basename "${1%.*}")

.. code-block:: console
    :caption: Run in terminal

    $ sh deploy.sh mylib.zip


Curl (win)
==========

.. code-block:: bat
    :caption: Create file: deploy.cmd
    :linenos:

    set endpoint=http://127.0.0.1:8000/api/v1/deploy/zip
    set token=NO

    curl ^
        -H "Token: %token%" ^
        --data-binary "@%1" ^
        %endpoint%/%~n1

.. code-block:: doscon
    :caption: Run in cmd.exe

    > deploy.cmd mylib.zip


Python
======

.. code-block:: python
    :caption: Create file: deploy.py
    :linenos:

    import pathlib
    import sys
    import ssl
    from urllib import request

    token = "NO"
    file = pathlib.Path(sys.argv[1])
    endpoint = f"http://127.0.0.1:8000/api/v1/deploy/zip/{file.stem}"

    req = request.Request(url=endpoint, data=file.read_bytes(), headers={"Token": token})

    context = ssl.create_default_context()
    if "insecure" in sys.argv:
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE

    res = request.urlopen(req, context=context)
    buffer = " "
    while buffer:
        buffer = res.readline(100)
        print(buffer.decode(errors="ignore"), end="")

.. code-block:: console
    :caption: Run in terminal

    $ python deploy.py mylib.zip
