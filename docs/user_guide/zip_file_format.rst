============
Zip archives
============

.. code styles that works for directory structure:
  fancy, kal, livescript, unicon, vim, zeek

Deployment
==========

Zip-files will be extracted directly to the :term:`deploy_root` directory when
they are deployed. In order for many different zip files to be extracted into
the same directory, this gives the following restrictions:

- A zip-file can only have one :term:`root` directory, so as not to overwrite a
  directory from another zip-file.

- The :term:`root` directory cannot contain a drive, device letter,
  or a leading slash.

- The :term:`root` directory name must be unique if many different zip files
  are to be deployed.

After extraction, all files under the :term:`deploy_root` -> :term:`root`
directory that do not belong to the zip-file will be deleted.

.. note:: 

    If the root directory contains extra info like a branch name or a version,
    this can be omitted from the actual name by using the
    :term:`Zip-Root` header when uploading/deploying.

Glossary
========

.. glossary:: 

    **Root**
        The term `root` is used to identify the root directory of a zip-file
        and the subdirectory it creates at :term:`deploy_root` when it is
        deployed. The root directory is used as the unique ID for the specific
        deployment.

Archive layout
==============

Example of a valid zip archive:

.. admonition:: Example
    :class: tip

    .. code-block:: kal

        /mylib/
            /README.rst
            /main.py
            /mypackage1/
                /__init__.py
                /__main__.py
            /mypackage2/
                /__init__.py
                /__main__.py


Following examples is **not** valid:

.. admonition:: Example 2
    :class: error

    .. code-block:: kal

        /README.rst
        /mylib/
            /main.py
            /mypackage1/
                /__init__.py
                /__main__.py
            /mypackage2/
                /__init__.py
                /__main__.py
    
.. admonition:: Example 3
    :class: error

    .. code-block:: kal

        /mylib1/
            /__init__.py
            /__main__.py
        /mylib2/
            /__init__.py
            /__main__.py


Deploy state
============

After a zip-file is deployed it will be referenced in a file called
``autodeploystate.ini``. This file is stored in the :term:`deploy_root`
directory.

.. code-block:: kal
    :caption: Example of directory structure

    /<deploy-root-dir>/
        /mylib/
        /otherlib/
        /autodeploystate.ini


.. admonition:: autodeploystate.ini example
    :class: note

    .. code-block:: ini

        [mylib]
        source_file = <cache-root-path>/ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5/post_data.zip
        destination_dir = <deploy-root-path>/mylib
        source_hash = ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5

        [otherlib]
        source_file = <cache-root-path>/faaf5acf512dec4f0b2a9976e758192431108c0deb82dc2084254296bac9def2/post_data.zip
        destination_dir = deploy-root-path>/otherlib
        source_hash = faaf5acf512dec4f0b2a9976e758192431108c0deb82dc2084254296bac9def2

``source_file``
    Full path to the unpacked zip-file.

``destination_dir``
    Full path to the :term:`root` directory.

``source_hash``
    sha256sum of the zip-file.

Cache
=====

When a zip-file is received from the :ref:`auto_fetch` routine or from
the REST-API it will be stored in a subdirectory under the :term:`cache_root`.
The subdirectory name is made from the sha256 sum of the zip-file. The name
of the zip-file will always be ``post_data.zip``. Metadata like HTTP-headers
is stored to ``post_metadata.json``.
``auto_delete.json`` contains necessary data for the auto-delete routine.


.. code-block:: kal
    :caption: Example of directory structure

    /cache/
        /204eaf942be391f75f148c2dd970279a45be1faea93398fae2bc091caa98e874/
            /post_data.zip
            /post_metadata.json
            /auto_delete.json
        /2e84f5e251ddd6f2e9d01c877a0b04e89dd0ed4beda6d328652ba672daeabe54/
            /post_data.zip
            /post_metadata.json
            /auto_delete.json
        /392658d457c8d32d4856e23bd643f88c324249d60913e4b36ef851846df3f290/
            /post_data.zip
            /post_metadata.json
            /auto_delete.json
        tags.ini

The ``tags.ini`` contains a mapping between the optional ``Zip-Tag`` header
and the sha256sum of the uploaded file.
