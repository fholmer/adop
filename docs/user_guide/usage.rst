=====
Usage
=====

.. _server_side:

Server side
===========

You can start the adop server by opening a terminal and type ``adop serve-api``.
To stop the server you can press :kbd:`ctrl+c` in the terminal window.

adop server can operate in two modes:

- **REST-API only mode**
    This is the default mode. In this mode the server will only act on
    commands received through the REST-API.

- **AUTO-FETCH mode**
    In this mode the server vil check a list of configured URLs and download and
    deploy zip-files on interval. The default interval is set to every 2 hours.
    The server will also act on commands from the REST-API.
    See :ref:`auto_fetch` for information on how to activate this mode.

Related topics:

- :ref:`reverse_proxy`
- :ref:`install_as_service`


Test the REST-API
-----------------

To check that the REST-API is working you can test it with a
tool called ``curl``. ``curl`` is usually pre-installed for both
windows and linux systems.

First you have to find the api token:

- Windows

  .. code-block:: doscon

    > type %USERPROFILE%\.adop\adop.ini | findstr write_token

- Linux

  .. code-block:: console

    $ cat ~/.adop/adop.ini | grep write_token

And then you can test the api using the returned token:

- Windows 

  .. code-block:: doscon

    > set ADOP_TOKEN=copy-paste-token-here
    > curl -H "Token: %ADOP_TOKEN%" http://127.0.0.1:8000/api/v1/test
    {
      "result": "It works",
      "result_code": 0
    }


- Linux

  .. code-block:: console

    $ export ADOP_TOKEN=copy-paste-token-here
    $ curl -H "Token: $ADOP_TOKEN" http://127.0.0.1:8000/api/v1/test
    {
      "result": "It works",
      "result_code": 0
    }

.. _client_side:

Client side
===========

Manage zip archives
-------------------

adop comes with a very primitive zip package installer. This tool is usually
not needed on the server because zip packages is automatically deployed from
the REST-API. But there may be clients that needs to install the same packages
that has been deployed to the server. First a ``requires.ini`` file have to be crafted.

.. code-block:: ini
    :caption: File: requires.ini

    [requires]
    mylib = sha256:ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5

Tags are also supported

.. code-block:: ini
    :caption: File: requires.ini

    [requires]
    mylib = tag:0.1.0

You also have to configure a remote in your ``adop.ini`` configuration file.
In this case however, we can just use the pre-configured ``local`` remote. This
remote is configured to install from your local REST-API instance if it is running.

Install
-------

Make sure that ``adop serve-api`` is running and install the packages from `local`:

.. code-block:: console

    $ adop zip install requires.ini --remote local
    Requires: mylib
              Checking mylib=sha256:ad9dbc5e...
              downloading mylib
              download complete
              verify data
              verify root dir
              verify zip data
              zip root: 'mylib'
              unpack zip data
              remove untracked files
              mylib installation complete.


.. tip:: 

  If the rest-api is running on another computer you have to configure it like this:

  .. code-block:: ini
      :caption: File: ~/.adop/adop.ini

      [remote:myserver]
      url = http://myserver:8000/api/v1
      token = NO
      insecure = 0

  .. code-block:: console

    $ adop zip install requires.ini --remote myserver

The zip archive should now be installed in the relative folder
``./autolibs/mylib``. See the ``[client]`` section in :ref:`default_configuration`
for more information.

Upload and deploy
-----------------

To upload and deploy the zip-file on "myserver":

  .. code-block:: console

    $ adop zip upload --deploy requires.ini --remote myserver
