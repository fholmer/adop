============
Installation
============

Instructions for different methods of installing adop:

- `From PyPI`_ (recommended)
- `Binary package`_
- `From source`_

From PyPI
=========

Prerequisites:

- Python 3.9 or newer

Open command line and and install using pip:

* Linux

  .. code-block:: console

    $ pip install adop[server]

* Windows

  .. code-block:: doscon

    > py -3 -m pip install adop[server]


adop is available as console script and library module:

.. code-block:: doscon

    > adop -h
    > python -m adop -h


.. _binary_package:

Binary package
==============

A *Windows installer* can be downloaded at 
`gitlab.com/fholmer/adop/-/packages
<https://gitlab.com/fholmer/adop/-/packages/?search%5B%5D=setup>`_

During installation you have to choose between *Install for all users*
(requires administator privileges) and *Install for me only*.

======================= ============================
Type                    Default path
======================= ============================
*Install for all users* %ProgramFiles%\\adop
*Install for me only*   %LOCALAPPDATA%\\Programs\\adop
======================= ============================

After installation is completed the executable is available at
``<selected-install-path>\bin\adop-cli.exe``.

.. code-block:: doscon

    > "%ProgramFiles%\adop\bin\adop-cli.exe" -h
    > "%LOCALAPPDATA%\Programs\adop\bin\adop-cli.exe" -h

From source
===========

Prerequisites:

- Python 3.9 or newer
- Git

Checkout with git:

.. code-block:: console

    $ git clone https://gitlab.com/fholmer/adop.git
    $ pip install .[server]

You can also install main branch directly from gitlab if git is not available:

.. code-block:: console

    $ pip install https://gitlab.com/fholmer/adop/-/archive/main/adop-main.zip#egg=adop[server]

