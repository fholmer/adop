=====================
REST API V1 Reference
=====================

.. code styles that works for HTTP headers:
  apacheconf, clean, cryptol, fstar, haskell, hspec, idris, logtalk
  monkey, ocaml, ooc, opa, prolog, pug, sass, savi, scala, shen
  slim, smalltalk, sophia, tcl,teal

Overview
========

========= ============================= ===================== ==================================
Method    Endpoint                      Headers               Description
========= ============================= ===================== ==================================
GET       /api/v1/test                  Token                 `Test api`_
GET       /api/v1/state                 Token                 `Deploy state`_
GET       /api/v1/state/<root>          Token                 `Deploy state`_
GET       /api/v1/tags/<root>           Token                 `Tags`_
GET       /api/v1/tags/<root>/<tag>     Token                 `Tags`_
GET       /api/v1/list/zip              Token                 `List zip-files`_
GET       /api/v1/list/zip/<root>       Token                 `List zip-files`_
POST/GET  /api/v1/trigger/fetch         Token                 `Trigger auto-fetch`_
POST      /api/v1/trigger/fetch/<root>  Token                 `Trigger auto-fetch`_
GET       /api/v1/download/zip/<root>   | Token,              `Download zip-file`_
                                        | Zip-Sha256/Zip-Tag
POST/PUT  /api/v1/upload/zip/<root>     | Token,              `Upload zip-file`_
                                        | Zip-Tag,
                                        | Zip-Root
POST/PUT  /api/v1/deploy/zip/<root>     | Token,              `Upload and deploy zip-file`_
                                        | Zip-Tag,
                                        | Zip-Root
GET       /api/v1/deploy/zip/<root>     | Token,              `Deploy preloaded zip-file`_
                                        | Zip-Sha256/Zip-Tag
GET       /api/v1/progress              Token                 `Unpacking progress`_
========= ============================= ===================== ==================================

**<root>**
    Name of the :term:`root` directory in the zip-file. Used to identify which
    zip-file is deployed.


Headers
-------

.. glossary:: 

  **Token**
      The authorization token for this API.
      
      .. admonition:: Example
        :class: note

        .. code-block:: slim

          Token: dcm5HvPu

  **Zip-Sha256**
      Content hash of the zip-file to deploy.

      .. admonition:: Example
        :class: note

        .. code-block:: slim

          Zip-Sha256: ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5

  **Zip-Tag**
      A tag that refers to a sha256sum

      .. admonition:: Example
        :class: note

        .. code-block:: slim

          Zip-Tag: 0.1.0

  **Zip-Root**
      Name of root directory in zip-file.
      Only needed if zip-root and <root> differs.

      .. admonition:: Example
        :class: note

        .. code-block:: slim

          Zip-Tag: mylib-0.1.0-adm64

Result
------

The result is encoded as a json object. Most endpoints will return an object
with ``result`` and ``result_code`` as keywords.

.. code-block:: console

    $ curl \
      -H "Token: dcm5HvPu" \
      http://127.0.0.1:8000/api/v1/test
    {
      "result": "It works", 
      "result_code": 0
    }

Endpoints that take a long time will stream a progress log until
the result is returned.

.. code-block:: console

    $ curl \
      -H "Content-Type: application/zip" \
      -H "Token: dcm5HvPu" \
      --data-binary "@work/mylib.zip" \
      http://127.0.0.1:8000/api/v1/deploy/zip/mylib
    // root: mylib
    // store data
    // verify data
    // verify root dir
    // verify zip data
    // zip root: 'mylib'
    // unpack zip data
    // remove untracked files
    {"root": "mylib", "result": "Success", "result_code": 0}


The Json specification does not support comments,
so the client must ignore lines prefixed with ``//`` before decoding.

.. code-block:: console

    $ curl \
      -H "Content-Type: application/zip" \
      -H "Token: dcm5HvPu" \
      --data-binary "@work/mylib.zip" \
      http://127.0.0.1:8000/api/v1/deploy/zip/mylib \
      | grep -v // \
      | python -m json.tool
    {
        "root": "mylib",
        "result": "Success",
        "result_code": 0
    }


Status and result codes
-----------------------

=========== ============ =================================================
HTTP status result_code  Descripton
=========== ============ =================================================
200         0            OK. Indicates that the request has succeeded.
200         1            Fail. The request has succeeded but result was
                         unsuccessful.
200         2            In progress. The request as been interrupted and
                         returned to early to give the final result code.
401         4            Unauthorized. Invalid token.
500         5            Internal Error
=========== ============ =================================================

Test api
========

Check that the API is up and running.

Request
-------

.. admonition:: HTTP Request

  .. code-block:: sass

    GET /api/v1/test
    Token: <token>

.. admonition:: Example
  :class: note

  .. code-block:: console

    $ curl \
      -H "Token: dcm5HvPu" \
      http://127.0.0.1:8000/api/v1/test
    {
        "result": "It works", 
        "result_code": 0
    }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    {
        "result": "It works", 
        "result_code": 0
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }


Deploy state
============

Returns the current sha256 sum for deployed zip-files.

Requests
--------

All zip-files

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/state
      Token: <token>

  .. admonition:: Example
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        http://127.0.0.1:8000/api/v1/state
      {
          "mylib": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5",
          "otherlib": "faaf5acf512dec4f0b2a9976e758192431108c0deb82dc2084254296bac9def2"
      }

A specific zip-file

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/state/<root>
      Token: <token>

  .. admonition:: Example
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        http://127.0.0.1:8000/api/v1/state/mylib
      {
          "mylib": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5"
      }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    {
        "mylib": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5",
        "otherlib": "faaf5acf512dec4f0b2a9976e758192431108c0deb82dc2084254296bac9def2"
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }

.. admonition:: HTTP status code 500 Internal Error
  :class: error

  .. code-block:: json

    {
        "result": "Internal Error",
        "result_code": 5
    }



Tags
====

Returns a mapping of tags and sha256sums

Requests
--------

All tags for given root

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/tags/<root>
      Token: <token>

  .. admonition:: Example
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        http://127.0.0.1:8000/api/v1/tags/mylib
      {
          "0.1.0": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5"
      }

A specific tag

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/tags/<root>/<tag>
      Token: <token>

  .. admonition:: Example
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        http://127.0.0.1:8000/api/v1/tags/mylib/0.1.0
      {
          "0.1.0": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5"
      }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    {
        "0.1.0": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5"
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }

.. admonition:: HTTP status code 500 Internal Error
  :class: error

  .. code-block:: json

    {
        "result": "Internal Error",
        "result_code": 5
    }


List zip-files
==============


Requests
--------

List all available zip files 

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/list/zip
      Token: <token>

  .. admonition:: Example
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        http://127.0.0.1:8000/api/v1/list/zip
      {
          "mylib": {
              "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5": {
                  "serial": 1665833411.5969493,
                  "sha256": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5",
                  "tag": "0.1.0"
              }
          },
          "otherlib": {
              "faaf5acf512dec4f0b2a9976e758192431108c0deb82dc2084254296bac9def2": {
                  "serial": 1665833411.5969493,
                  "sha256": "faaf5acf512dec4f0b2a9976e758192431108c0deb82dc2084254296bac9def2",
                  "tag": "0.2.0"
              }
          }
      }

A specific root

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/list/zip/<root>
      Token: <token>

  .. admonition:: Example
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        http://127.0.0.1:8000/api/v1/list/zip/mylib
      {
          "mylib": {
              "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5": {
                  "serial": 1665833411.5969493,
                  "sha256": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5",
                  "tag": "0.1.0"
              }
          }
      }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    {
        "mylib": {
            "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5": {
                "serial": 1665833411.5969493,
                "sha256": "ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5",
                "tag": "0.1.0"
            }
        }
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }

.. admonition:: HTTP status code 500 Internal Error
  :class: error

  .. code-block:: json

    {
        "result": "Internal Error",
        "result_code": 5
    }


Trigger auto-fetch
==================

Trigger the auto-fetch routine if it is enabled.
See :ref:`auto_fetch` for information on how to activate this mode.

Request
-------

.. admonition:: HTTP Request

  .. code-block:: sass

    GET /api/v1/trigger/fetch
    Token: <token>

  Or

  .. code-block:: sass

    POST /api/v1/trigger/fetch/<root>
    Token: <token>

  Optional headers:

  .. code-block:: sass

    X-Gitlab-Token: <secret>
    X-Hub-Signature-256: <sha256=signature>
    HTTP_X_GITEA_SIGNATURE: <signature>

.. admonition:: Example
  :class: note

  .. code-block:: console

    $ curl \
      -H "Token: dcm5HvPu" \
      http://127.0.0.1:8000/api/v1/trigger/fetch
    {
        "result": "OK", 
        "result_code": 0
    }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    {
        "result": "OK", 
        "result_code": 0
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }

.. admonition:: HTTP status code 500 Internal Error
  :class: error

  .. code-block:: json

    {
        "result": "Internal Error",
        "result_code": 5
    }

.. admonition:: HTTP status code 503 OK
  :class: error

  .. code-block:: json

    {
        "result": "auto_fetch is not available", 
        "result_code": 1
    }


Download zip-file
=================

Download the latest zip-file for the given root.

Requests
--------

Get the latest file

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/download/zip/<root>
      Token: <token>

  .. admonition:: Example
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        -o mylib.zip \
        http://127.0.0.1:8000/api/v1/download/zip/mylib
      $ ls mylib.zip
      mylib.zip

Get a specific file

  .. admonition:: HTTP Request

    .. code-block:: sass

      GET /api/v1/download/zip/<root>
      Token: <token>
      Zip-Sha256: <sha256>

    Or

    .. code-block:: sass

      GET /api/v1/download/zip/<root>
      Token: <token>
      Zip-Tag: <tag>

  .. admonition:: Example <sha256>
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        -H "Zip-Sha256: ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5" \
        -o mylib.zip \
        http://127.0.0.1:8000/api/v1/download/zip/mylib
        
      $ sha256sum mylib.zip
      5d9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5  mylib.zip


  .. admonition:: Example <tag>
    :class: note

    .. code-block:: console

      $ curl \
        -H "Token: dcm5HvPu" \
        -H "Zip-Tag: 0.1.0" \
        -o mylib.zip \
        http://127.0.0.1:8000/api/v1/download/zip/mylib
        
      $ sha256sum mylib.zip
      5d9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5  mylib.zip

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  Response returns the zip-file as binary data.

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }

.. admonition:: HTTP status code 500 Internal Error
  :class: error

  .. code-block:: json

    {
        "result": "Internal Error",
        "result_code": 5
    }

Upload zip-file
===============

Upload a zip-file without deploying it.

Request
-------

.. admonition:: HTTP Request

  .. code-block:: sass

    POST /api/v1/upload/zip/<root>
    Token: <token>
  
  Optional headers:

  .. code-block:: sass

    Zip-Tag: <tag>
    Zip-Root: <zip-root>

.. admonition:: Example
  :class: note

  .. code-block:: console

    $ curl \
        -H "Token: dcm5HvPu" \
        -H "Zip-Tag: 0.1.0" \
        --data-binary "@mylib.zip" \
        http://127.0.0.1:8000/api/v1/upload/zip/mylib
      // root: mylib
      // store data
      // verify data
      // verify root dir
      {
          "root": "mylib",
          "result": "Success",
          "result_code": 0
      }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    // root: mylib
    // store data
    // verify data
    // verify root dir
    {
        "root": "mylib",
        "result": "Success",
        "result_code": 0
    }

.. admonition:: HTTP status code 200 OK
  :class: warning

  .. code-block:: json

    // root: mylib
    // store data
    // verify data
    {
        "result": "Fail", 
        "result_code": 1
    }

.. admonition:: HTTP status code 200 OK
  :class: warning

  .. code-block:: json

    {
        "root": "mylib",
        "result": "In progress",
        "result_code": 2
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }


Upload and deploy zip-file
==========================

Upload and deploy a zip-file.

Request
-------

.. admonition:: HTTP Request

  .. code-block:: sass

    POST /api/v1/deploy/zip/<root>
    Token: <token>

  Optional headers:

  .. code-block:: sass

    Zip-Tag: <tag>
    Zip-Root: <zip-root>

.. admonition:: Example
  :class: note

  .. code-block:: console

    $ curl \
        -H "Token: dcm5HvPu" \
        -H "Zip-Tag: 0.1.0" \
        --data-binary "@mylib.zip" \
        http://127.0.0.1:8000/api/v1/deploy/zip/mylib
      // root: mylib
      // store data
      // verify data
      // verify root dir
      // verify zip data
      // zip root: 'mylib'
      // unpack zip data
      // remove untracked files
      {
          "root": "mylib",
          "result": "Success",
          "result_code": 0
      }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    // root: mylib
    // store data
    // verify data
    // verify root dir
    // verify zip data
    // zip root: 'mylib'
    // unpack zip data
    // remove untracked files
    {
        "root": "mylib",
        "result": "Success",
        "result_code": 0
    }

.. admonition:: HTTP status code 200 OK
  :class: warning

  .. code-block:: json

    // root: mylib
    // store data
    // verify data
    {
        "result": "Fail", 
        "result_code": 1
    }

.. admonition:: HTTP status code 200 OK
  :class: warning

  .. code-block:: json

    {
        "root": "mylib",
        "result": "In progress",
        "result_code": 2
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }



Deploy preloaded zip-file
=========================

Deploy a preloaded zip-file.

Request
-------

.. admonition:: HTTP Request

  .. code-block:: sass

    GET /api/v1/deploy/zip/<root>
    Token: <token>
    Zip-Sha256: <sha256>

  Or

  .. code-block:: sass

    GET /api/v1/deploy/zip/<root>
    Token: <token>
    Zip-Tag: <tag>

.. admonition:: Example
  :class: note

  .. code-block:: console

    $ curl \
        -H "Token: dcm5HvPu" \
        -H "Zip-Sha256: ad9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5" \
        http://127.0.0.1:8000/api/v1/deploy/zip/mylib
      // root: mylib
      // verify root dir
      // verify zip data
      // zip root: 'mylib'
      // unpack zip data
      // remove untracked files
      {
          "root": "mylib",
          "result": "Success",
          "result_code": 0
      }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    // root: mylib
    // verify root dir
    // verify zip data
    // zip root: 'mylib'
    // unpack zip data
    // remove untracked files
    {
        "root": "mylib",
        "result": "Success",
        "result_code": 0
    }

.. admonition:: HTTP status code 200 OK
  :class: warning

  .. code-block:: json

    // root: mylib
    // verify root dir
    // verify zip data
    {
        "result": "Fail", 
        "result_code": 1
    }

.. admonition:: HTTP status code 200 OK
  :class: warning

  .. code-block:: json

    {
        "root": "mylib",
        "result": "In progress",
        "result_code": 2
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }



Unpacking progress
==================

Returns zip-file unpacking progress.

Request
-------

.. admonition:: HTTP Request

  .. code-block:: sass

    GET /api/v1/progress
    Token: <token>

.. admonition:: Example
  :class: note

  .. code-block:: console

    $ curl \
      -H "Token: dcm5HvPu" \
      http://127.0.0.1:8000/api/v1/progress
    {
      "mylib": {
        "log": [
          "root: mylib", 
          "store data", 
          "verify data", 
          "verify root dir"
        ], 
        "result": "Success", 
        "result_code": 0, 
        "root": "mylib"
      }
    }

Responses
---------

.. admonition:: HTTP status code 200 OK
  :class: tip

  .. code-block:: json

    {
      "mylib": {
        "log": [
          "root: mylib", 
          "store data", 
          "verify data", 
          "verify root dir"
        ], 
        "result": "Success", 
        "result_code": 0, 
        "root": "mylib"
      }
    }

.. admonition:: HTTP status code 401 Unauthorized
  :class: error

  .. code-block:: json

    {
        "result": "Invalid token",
        "result_code": 4
    }

.. admonition:: HTTP status code 500 Internal Error
  :class: error

  .. code-block:: json

    {
        "result": "Internal Error",
        "result_code": 5
    }
