=============
CLI Reference
=============


Command-line options for ``adop``.

adop
====

Print the sub commands and exit.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop -h
        usage: adop [-h] [-V] {serve-api,serve-api-v1,zip,zip-v1,config} ...

        options:
          -h, --help            show this help message and exit
          -V, --version         print version and exit

        Commands:
          Additional help for commands: {command} --help

          {serve-api,serve-api-v1,zip,zip-v1}
            serve-api           REST API latest version
            serve-api-v1        REST API v1 (this is the latest version)
            zip                 Zip commands, latest version
            zip-v1              Zip commands v1 (this is the latest version)
            config              Config commands

-V, --version
    Print version and exit


adop serve-api
==============

Serve the REST-API. To stop the server you can press :kbd:`ctrl+c`
in the terminal window.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop serve-api -h
        usage: adop serve-api [-h] [-c CONFIG] [--cwd CWD] [-b HOST] [-p PORT]

        options:
          -h, --help            show this help message and exit
          -c CONFIG, --config CONFIG
                                Path to config [default: ~/.adop/adop.ini]
          --cwd CWD             Work dir [default: .]
          -b HOST, --bind HOST  Specify alternate bind address [default: 127.0.0.1]
          -p PORT, --port PORT  Specify alternate port [default: 8000]

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created with
    default options.

--cwd
    ``[default: .]``
    Work directory.

-b, --bind
    ``[default: 127.0.0.1]``
    Specify alternate bind address. This will override configured bind address.

-p, --port
    ``[default: 8000]``
    Specify alternate port. This will override the configured port.


adop zip
========

Print the sub commands and exit.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop zip -h
        usage: adop zip [-h] {install,upload,import} ...

        optional arguments:
        -h, --help  show this help message and exit

        Commands:
        Additional help for commands: {command} --help

        {install,upload,import}
            install   Install zip-packages
            upload    Upload zip-packages
            import    Import zip-packages into the local cache


adop zip install
================

Get the referenced zip-file from ``REMOTE`` and unpack its content into
``INSTALL`` location.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop zip install --help
        usage: adop zip install [-h] [-c CONFIG] [--cwd CWD] [-r REMOTE] [-i INSTALL] [file]

        positional arguments:
        file                    Install from the given requires.ini file

        optional arguments:
        -h, --help              show this help message and exit
        -c CONFIG, --config CONFIG
                                Path to config [default: ~/.adop/adop.ini]
        --cwd CWD               Work dir [default: .]
        -r REMOTE, --remote REMOTE
                                Install from the given remote.
                                Remotes have to be configured in adop.ini.
                                [default: local]
        -i INSTALL, --install INSTALL
                                Install to the given location. Locations have to be
                                configured in adop.ini. [default: basedir]

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created with
    default options.

--cwd
    ``[default: .]``
    Work directory.

-r REMOTE, --remote REMOTE
    ``[default: local]``
    Install from the given remote. Remotes have to be configured in adop.ini.

    .. code-block:: ini
        :caption: ~/.adop/adop.ini

        [remote:myserver]
        url = https://adop.myserver.local/api/v1
        token = NO
        insecure = 0

    .. code-block:: console
        :caption: terminal

        $ adop zip install requires.ini --remote myserver
        Requires: mylib
          Checking mylib=sha256:5d9dbc5e...
          mylib already installed.
    
    This option be specified multiple times:

    .. code-block:: console
        :caption: terminal

        $ adop zip install requires.ini -r myserver -r otherserver
    
    If the command succeeds on the first remote then the other remotes
    is not checked. The returncode will be 0 as long as the command succeeds
    on one of the remotes.

-i INSTALL, --install INSTALL
    ``[default: basedir]``
    Install to the given location. Locations have to be configured in adop.ini.

    .. code-block:: ini
        :caption: ~/.adop/adop.ini

        [install:mylibs]
        install_root = ../mylibs/auto
        cache_root = ../mylibs/.cache

    .. code-block:: console
        :caption: terminal

        $ adop zip install requires.ini --remote myserver --install mylibs
        Requires: mylib
          Checking mylib=sha256:5d9dbc5e...
          mylib already installed.


adop zip upload
===============

Get the referenced zip-file from the local cache and upload the file
to ``REMOTE``.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop zip upload --help
        usage: adop zip upload [-h] [-c CONFIG] [--cwd CWD] [-d] [--force-upload]
                               [--force-deploy] [-r REMOTE] [-i INSTALL] [file]

        positional arguments:
        file                  Upload from the given requires.ini file to REMOTE

        optional arguments:
        -h, --help            show this help message and exit
        -c CONFIG, --config CONFIG
                              Path to config [default: ~/.adop/adop.ini]
        --cwd CWD             Work dir [default: .]
        -d, --deploy          Upload and deploy the zipfile on REMOTE.
                              Do not deploy if already uploaded.
        --force-upload        Force upload zipfile to REMOTE. Deploy on REMOTE if --deploy
                              is set.
        --force-deploy        Force a deploy request on REMOTE.
                              Only upload zipfile if missing on REMOTE
        -r REMOTE, --remote REMOTE
                              Upload to the given remote. Remotes have to be configured
                              in adop.ini. [default: local]
        -i INSTALL, --install INSTALL
                              Upload from the given location. Locations have to be
                              configured in adop.ini. [default: basedir]



-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created with
    default options.

--cwd
    ``[default: .]``
    Work directory.

-d, --deploy
    Upload and deploy the zipfile on REMOTE. Do not deploy if already uploaded.

--force-upload
    Force upload zipfile to REMOTE. Deploy on REMOTE if ``--deploy`` is set.

--force-deploy
    Force a deploy request on REMOTE. Only upload zipfile if missing on REMOTE

-r REMOTE, --remote REMOTE
    ``[default: local]``
    Upload to the given remote. Remotes have to be configured in adop.ini.
    This option be specified multiple times.


-i INSTALL, --install INSTALL
    ``[default: basedir]``
    Upload from the given location. Locations have to be configured in adop.ini.


adop zip import
===============

Get the referenced zip-file and update the local cache and unpack its content into
``INSTALL`` location.

-h, --help
    Print help message and exit

    .. code-block:: text

        usage: adop zip import [-h] [-c CONFIG] [--cwd CWD] [-i INSTALL] [-r REMOTE] -p
                       ROOT [-f ZIPFILE] [--insecure] -t ZIPTAG [-z ZIPROOT]
                       [requiresfile]

        positional arguments:
        requiresfile            Update the given requires.ini file with imported zip
                                package

        optional arguments:
        -h, --help              show this help message and exit
        -c CONFIG, --config CONFIG
                                Path to config [default: ~/.adop/adop.ini]
        --cwd CWD               Work dir [default: .]
        -i INSTALL, --install INSTALL
                                Import into to the given "install-location"-cache.
                                Locations have to be configured in adop.ini. [default:
                                basedir]
        -r REMOTE, --remote REMOTE
                                Import from the given remote. Remotes have to be
                                configured in adop.ini. [default: local]
        -p ROOT, --package ROOT
                                The root name of the import
        -f ZIPFILE, --zip-file ZIPFILE
                                Path or URL of the zip-file to be imported. This option
                                overrides the --remote option
        --insecure              Skip SSL verification for the --zip-file option.
        -t ZIPTAG, --zip-tag ZIPTAG
                                A tag to associate with the import
        -z ZIPROOT, --zip-root ZIPROOT
                                Name of the root dir in zip-file. Use if dir name differs
                                from --package name.

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created with
    default options.

--cwd
    ``[default: .]``
    Work directory.

-r REMOTE, --remote REMOTE
    ``[default: local]``
    Import from the given remote. Remotes have to be configured in ``adop.ini``.

-i INSTALL, --install INSTALL
    ``[default: basedir]``
    Import to the given location. Locations have to be configured in ``adop.ini``.

-p ROOT, --package ROOT
    The root name of the import

-f ZIPFILE, --zip-file ZIPFILE
    Path or URL of the zip-file to be imported. This option
    overrides the ``--remote`` option

--insecure
    Skip SSL verification for the ``--zip-file`` option.

-t ZIPTAG, --zip-tag ZIPTAG
    A tag to associate with the import

-z ZIPROOT, --zip-root ZIPROOT
    Name of the root dir in zip-file. Use if dir name differs
    from ``--package`` name.

adop config
===========

Print the sub commands and exit.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config --help
        usage: adop config [-h] {init,set,get,list,generate-token,import,open,add-remote,add-install,add-autofetch} ...

        optional arguments:
        -h, --help           show this help message and exit

        Commands:
        Additional help for commands: {command} --help

        {init,set,get,list,generate-token,import,open,add-remote,add-install,add-autofetch}
            init                init options
            set                 set option
            get                 get option
            list                list options
            generate-token      Generate a token
            import              import options
            open                open options
            add-remote          Add a remote named <name> for the server at <URL>
            add-install         Add a install location named <name> at <install_root> with a cache at <cache_root>
            add-autofetch       Add a autofetch source named <name>

adop config init
================

Create the config file with default options. A token is generated
for the ``write_token`` and ``read_token`` options.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config init --help
        usage: adop config init [-h] [-c CONFIG_FILE] [--cwd CWD]

        optional arguments:
        -h, --help            show this help message and exit
        -c CONFIG_FILE, --config CONFIG_FILE
                              Path to config [default: ~/.adop/adop.ini]
        --cwd CWD             Work dir [default: .]

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file exists the command will fail with return code 1.

--cwd
    ``[default: .]``
    Work directory.

-m, --merge
    Write default values to config file, keeping existing options.

adop config set
===============

Change or add an option in the config file. The config file is created
if it does not already exists. The config file will not be created
with default options. See `adop config init`_.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config set --help
        usage: adop config set [-h] [-c CONFIG_FILE] [--cwd CWD] section option value

        positional arguments:
        section               section-name in adop.ini
        option                option-name in adop.ini
        value                 new value for given option in given section

        optional arguments:
        -h, --help            show this help message and exit
        -c CONFIG_FILE, --config CONFIG_FILE
                              Path to config [default: ~/.adop/adop.ini]
        --cwd CWD             Work dir [default: .]

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created.

--cwd
    ``[default: .]``
    Work directory.

section
    section-name in ``CONFIG_FILE``

option
    option-name in ``CONFIG_FILE``

value
    new value for given option in given section.


adop config get
===============

Prints the value of a option and exit. If the option is not found in the
config file, the command fail with return code 1. Use ``-a`` to search in the
default values. 

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config get --help
        usage: adop config get [-h] [-c CONFIG_FILE] [--cwd CWD] [-a] section option

        positional arguments:
        section               section-name in adop.ini
        option                option-name in adop.ini

        optional arguments:
        -h, --help            show this help message and exit
        -c CONFIG_FILE, --config CONFIG_FILE
                              Path to config [default: ~/.adop/adop.ini]
        --cwd CWD             Work dir [default: .]
        -a, --all             Include default values in result

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists,
    the command will fail with return code 1.

--cwd
    ``[default: .]``
    Work directory.

-a, --all
    Include default values in the search.

section
    section-name in ``CONFIG_FILE``

option
    option-name in ``CONFIG_FILE``


adop config list
================

Prints the options found in config file and exit. Use ``-all`` to print the
default options as well.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config list --help
        usage: adop config list [-h] [-c CONFIG_FILE] [--cwd CWD] [-a]

        optional arguments:
        -h, --help            show this help message and exit
        -c CONFIG_FILE, --config CONFIG_FILE
                                Path to config [default: ~/.adop/adop.ini]
        --cwd CWD             Work dir [default: .]
        -a, --all             Include default values in result

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists,
    the command will fail with return code 1.

--cwd
    ``[default: .]``
    Work directory.

-a, --all
    Include default values in result

adop config generate-token
==========================

Prints a new generated token and exit

-h, --help
    Print help message and exit


adop config import
==================

Update the config file with the given import file. If the
import file is ``-`` the file will be imported from stdin.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ usage: adop config import [-h] [-c CONFIG_FILE] [--cwd CWD] file

        positional arguments:
        file                  import given ini file

        optional arguments:
        -h, --help            show this help message and exit
        -c CONFIG_FILE, --config CONFIG_FILE
                                Path to config [default: ~/.adop/adop.ini]
        --cwd CWD             Work dir [default: .]

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists,
    the command will fail with return code 1.

--cwd
    ``[default: .]``
    Work directory.

file
    name of the file. Set to ``-`` to read from stdin.

    Example 1:

    .. code-block:: console

        $ adop config import ./importfilename.ini
        Imported ./importfilename.ini into ~/.adop/adop.ini

    Example 2:

    .. code-block:: console

        $ cat ./importfilename.ini | adop config import -
        Imported - into ~/.adop/adop.ini


adop config open
================

Open the config file in a text editor

-h, --help
    Print help message and exit

    .. code-block:: text

        usage: adop config open [-h] [-c CONFIG_FILE] [--cwd CWD]

        optional arguments:
        -h, --help              show this help message and exit
        -c CONFIG_FILE, --config CONFIG_FILE
                                Path to config [default: ~/.adop/adop.ini]
        --cwd CWD               Work dir [default: .]

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to config-file.

--cwd
    ``[default: .]``
    Work directory.


adop config add-remote
======================

Add a remote named <name> for the server at <URL>.
The config file is created if it does not already exists. The config file will
not be created with default options. See `adop config init`_.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config add-remote -h
        usage: adop config add-remote [-h] [-p] [-e TOKEN_ENV] [-k TOKEN_KEYRING] [-i] [-c CONFIG_FILE] [--cwd CWD] name url

        positional arguments:
          name                  remote name
          url                   remote url

        optional arguments:
          -h, --help            show this help message and exit
          -p, --token-prompt    prompt for a remote token
          -e TOKEN_ENV, --token-env TOKEN_ENV
                                get the remote token from environment variable
          -k TOKEN_KEYRING, --token-keyring TOKEN_KEYRING
                                get the remote token from keyring
          -i, --insecure        Allow untrusted certificates
          -c CONFIG_FILE, --config CONFIG_FILE
                                Path to config [default: ~/.adop/adop.ini]
          --cwd CWD             Work dir [default: .]

name
    remote name in ``CONFIG_FILE``

url
    remote url

-p, --token-prompt
    prompt for a remote token

-e TOKEN_ENV, --token-env TOKEN_ENV
    get the remote token from environment variable

-k TOKEN_KEYRING, --token-keyring TOKEN_KEYRING
    get the remote token from keyring

-i, --insecure
    Allow untrusted certificates

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created.

--cwd
    ``[default: .]``
    Work directory.


adop config add-install
=======================

Add a install location named <name> at <install_root> with a cache at <cache_root.
The config file is created if it does not already exists. The config file will
not be created with default options. See `adop config init`_.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config add-install -h
        usage: adop config add-install [-h] [-c CONFIG_FILE] [--cwd CWD] name install_root cache_root

        positional arguments:
          name                  install name
          install_root          install root path
          cache_root            cache root path

        optional arguments:
          -h, --help            show this help message and exit
          -c CONFIG_FILE, --config CONFIG_FILE
                                Path to config [default: ~/.adop/adop.ini]
          --cwd CWD             Work dir [default: .]

name
    install name in ``CONFIG_FILE``

install_root
    install root path

cache_root
    cache root path

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created.

--cwd
    ``[default: .]``
    Work directory.


adop config add-autofetch
=========================

Add a autofetch source named <name>.
The config file is created if it does not already exists. The config file will
not be created with default options. See `adop config init`_.

-h, --help
    Print help message and exit

    .. code-block:: text

        $ adop config add-autofetch -h
        usage: adop config add-autofetch [-h] [-H HEADERS] [-z ZIP_ROOT] [-c CONFIG_FILE] [--cwd CWD] name root check_url payload_url

        positional arguments:
          name                  autofetch name
          root                  root name
          check_url             check_url
          payload_url           payload_url

        optional arguments:
          -h, --help            show this help message and exit
          -H HEADERS, --headers HEADERS
                                headers
          -z ZIP_ROOT, --zip-root ZIP_ROOT
                                zip_root
          -c CONFIG_FILE, --config CONFIG_FILE
                                Path to config [default: ~/.adop/adop.ini]
          --cwd CWD             Work dir [default: .]

name
    autofetch name in ``CONFIG_FILE``

root
    root name

check_url
    check_url

payload_url
    payload_url

-H HEADERS, --headers HEADERS
    headers

-z ZIP_ROOT, --zip-root ZIP_ROOT
    zip_root

-c, --config
    ``[default: ~/.adop/adop.ini]``
    Path to a config-file. If the file does not exists it will be created.

--cwd
    ``[default: .]``
    Work directory.
