================
adop development
================

Get the source
==============

.. code-block:: console

    $ git clone https://gitlab.com/fholmer/adop.git

Setup dev environment
=====================

- Linux

  .. code-block:: console

    $ python3 -m venv .venv
    $ source .venv/bin/activate
    $ pip install -e .
    $ pip install -r dev-requirements.txt -r requirements.txt
    $ adop config init -c work/adop.ini
    $ adop config set -c work/adop.ini server deploy_root ./work/auto
    $ adop config set -c work/adop.ini server cache_root ./work/cache 

- Windows

  .. code-block:: doscon

    > py -3 -m venv .venv
    > .venv\Scripts\activate
    > pip install -e .
    > pip install -r dev-requirements.txt -r requirements.txt
    > adop config init -c work/adop.ini
    > adop config set -c work/adop.ini server deploy_root ./work/auto
    > adop config set -c work/adop.ini server cache_root ./work/cache 


Run tests
=========

.. code-block:: console

    $ pip install pytest
    $ pytest


Code coverage
=============

.. code-block:: console
    
    $ pip install pytest-cov
    $ pytest --cov=adop --cov-report=xml --cov-report=term


Run dev server
==============

.. code-block:: console

    $ adop serve-api -c work/adop.ini

Test clients
============

- Linux 

  .. code-block:: console

    $ source scripts/activate_env_local.sh
    $ sh scripts/curl_test.sh
    $ sh scripts/curl_state.sh

- Windows

  .. code-block:: doscon

    > scripts\activate_env_local.cmd
    > scripts\curl_test.cmd
    > scripts\curl_state.cmd


Code formatting
===============

.. code-block:: console

    $ pip install isort black flake8
    $ isort adop tests
    $ black adop tests
    $ flake8 adop tests


Build
=====


Wheel
-----

.. code-block:: console

    $ pip install wheel
    $ python setup.py bdist_wheel


Windows binary
--------------

.. code-block:: doscon

    > pip install pyinstaller
    > python adop-generate-file-version.py
    > pyinstaller --name adop-cli --version-file file_version_info.txt adop-cli.py


Dist
====

Wheel
-----

.. code-block:: console

    $ pip install twine
    $ twine upload dist/*.whl


Windows binary
--------------

.. code-block:: doscon

    > ISCC.exe adop-setup.iss

Docs
====

Build html
----------

.. code-block:: console

    $ cd docs
    $ make html

Update API docs
---------------

.. code-block:: console

    $ cd docs
    $ sphinx-apidoc -feo developer_guide/api/ ../adop
