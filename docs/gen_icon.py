import sys
import math
import re

import adop

version = re.match(r"(\d+\.){2}\d+", adop.__version__).group(0)

colors = [
    "#E14D2A",
    "#FD841F",
    "#3E6D9C",
    "#001253",
]

tofile = None

w = 60
h = 60
if "tofile" in sys.argv:
    tofile = f"./_static/img/logo/{version}/favicon.svg"


def favicon():
    t = '<circle cx="{x}" cy="{y}" r="{r}" stroke="" fill="{c}" />'
    return [
        t.format(x=30, y=30, r=30, c=colors[0]),
        t.format(x=20, y=20, r=10, c=colors[1]),
        t.format(x=30, y=30, r=10, c=colors[2]),
        t.format(x=40, y=40, r=10, c=colors[3]),
    ]


def head():
    return f"""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="{w}" height="{h}" viewBox="0 0 {w} {h}" xml:space="preserve">
"""


def tail():
    return """
</svg>
"""


if tofile:
    print(f"write to {tofile}")
    with open(tofile, "w") as f:
        print(head(), "\n".join(favicon()), tail(), file=f)
else:
    print(head(), "\n".join(favicon()), tail(), end="")
