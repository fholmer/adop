.. adop documentation master file, created by
   sphinx-quickstart on Thu Aug  4 09:28:00 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================================
Welcome to adop's documentation.
================================

.. image:: _static/img/logo/banner.svg

.. include:: ../README.rst
   :start-after: adop
   :end-before: Installation

Table of contents
=================

.. toctree::
   :maxdepth: 2
   :caption: User guide:
   :titlesonly:

   Quickstart <user_guide/quickstart>
   user_guide/installation
   user_guide/usage
   user_guide/zip_file_format
   user_guide/configuration
   user_guide/integrations
   user_guide/api_cli
   user_guide/clients
   user_guide/api_rest
   changelog

.. toctree::
   :maxdepth: 1
   :caption: Developer guide:
   :titlesonly:

   developer_guide/howto
   adop API Reference <developer_guide/api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
