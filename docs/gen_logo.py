import random
import sys
import math
import re

import adop

version = re.match(r"(\d+\.){2}\d+", adop.__version__).group(0)

random.seed(version)

colors = [
    "#E14D2A",
    "#FD841F",
    "#3E6D9C",
    "#001253",
]

tofile = None

if "banner" in sys.argv:
    w = 1080
    h = 1080 // 10
    if "tofile" in sys.argv:
        tofile = f"./_static/img/logo/{version}/banner.svg"
else:
    w = 540
    h = 540
    if "tofile" in sys.argv:
        tofile = f"./_static/img/logo/{version}/logo.svg"

x_max = w
y_max = h
w_h_max = w // 3  # Largest rectangle
r_max = w // 5  # Largest circle
f_max = int((math.sqrt(h) / math.sqrt(w)) * w)


def rect(i):
    x, y, w, h = map(random.randrange, [x_max, y_max, w_h_max, w_h_max])
    c = i % len(colors)
    return (
        f'<rect x="{x}" y="{y}" width="{w}" height="{h}" transform="rotate({i} {x},{y})" '
        f'style="fill:{colors[c]}; stroke:rgba(0, 0, 0, 0)"'
        "/>"
    )


def circle(i):
    x, y, r = map(random.randrange, [x_max, y_max, r_max])
    c = i % len(colors)
    return (
        f'<ellipse cx="{x}" cy="{y}" rx="{r+i}" ry="{r-i}" transform="rotate({i} {x},{y})" '
        f'style="fill:{colors[c]};stroke:rgba(0, 0, 0, 0)" '
        "/>"
    )


def head():
    return f"""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" width="{w}" height="{h}" viewBox="0 0 {w} {h}" xml:space="preserve">
<rect width="{w}" height="{h}" style="fill:{colors[0]};  stroke:rgba(0, 0, 0, 0)" />"""


def tail():
    return f"""<text x="400" y="150" font-family="Georgia, Times, serif" font-size="{int(f_max * 0.22)}" font-weight="900" style="fill:white;" >
<tspan x="{int(w * 0.61)}" y="{int(h * 0.28)}">ad</tspan>
<tspan x="{int(w * 0.68)}" y="{int(h * 0.28) + int(f_max * 0.15)}">op</tspan>
<tspan x="{int(w * 0.97)}" y="{int(h * 0.97)}"  text-anchor="end">{version}</tspan>
</text>
</svg>"""


shapes = [rect(20 - i) for i in range(40)] + [circle(20 - i) for i in range(40)]
random.shuffle(shapes)

if tofile:
    print(f"write to {tofile}")
    with open(tofile, "w") as f:
        print(head(), "\n".join(shapes), tail(), file=f)
else:
    print(head(), "\n".join(shapes), tail())
