import json
from unittest.mock import Mock, mock_open, patch

import pytest

from adop import exceptions, store_payload


@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_store(_open, _mkdir):
    post_data, cache = store_payload.store(b"TEST", "FAKEDIR", "root", "tag", {})

    assert len(cache) > 0
    assert post_data.parent.name == cache

    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called()
    assert _open.call_count == 3

    handle = _open()
    assert handle.write.call_count == 3


@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_store_no_root(_open, _mkdir):
    with pytest.raises(exceptions.Fail):
        store_payload.store(b"TEST", "FAKEDIR", "", "", {})


@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_store_no_content(_open, _mkdir):
    with pytest.raises(exceptions.Fail):
        store_payload.store(b"", "FAKEDIR", "root", "tag", {})


@patch("pathlib.Path.mkdir")
def test_find_file_from_headers_errors(_mkdir):
    with pytest.raises(exceptions.Fail):  # no header
        store_payload.find_file_from_headers("FAKEDIR", "mylib", {})

    with pytest.raises(exceptions.Fail):  # no header
        store_payload.find_file_from_headers("FAKEDIR", "mylib", {"Zip-Sha256": ""})

    with pytest.raises(exceptions.Fail):  # no header
        store_payload.find_file_from_headers("FAKEDIR", "mylib", {"Zip-Sha256": "1 1"})

    with pytest.raises(exceptions.Fail):  # header not sha256
        store_payload.find_file_from_headers("FAKEDIR", "mylib", {"Zip-Sha256": "11"})


@patch("pathlib.Path.mkdir")
def test_find_file_from_headers_file_not_found(_mkdir):
    with pytest.raises(exceptions.Fail):  # file not found
        store_payload.find_file_from_headers(
            "FAKEDIR", "mylib", {"Zip-Sha256": "A" * 64}
        )


@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text", return_value="{}")
@patch("pathlib.Path.exists")
def test_find_file_from_headers_zip_sha256(_exists, _read_text, _mkdir):
    _exists.return_value = True
    post_data, cache, tag = store_payload.find_file_from_headers(
        "FAKEDIR", "mylib", {"Zip-Sha256": "A" * 64}
    )
    assert cache == "A" * 64
    assert tag == ""
    assert post_data.parent.name == cache


@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text")
@patch("pathlib.Path.exists")
def test_find_file_from_headers_zip_tag(_exists, _read_text, _mkdir):
    _exists.return_value = True
    _read_text.side_effect = [
        f"[mylib]\n0.1.2 = {'A'*64}",
        json.dumps({"tag": "0.1.2", "root": "mylib"}),
    ]
    post_data, cache, tag = store_payload.find_file_from_headers(
        "FAKEDIR", "mylib", {"Zip-Tag": "0.1.2"}
    )
    assert cache == "A" * 64
    assert tag == "0.1.2"
    assert post_data.parent.name == cache


@patch("pathlib.Path.glob")
@patch("shutil.rmtree")
def test_auto_delete_empty(_rm, _glob):
    _glob.return_value = []

    res = list(
        res
        for res in store_payload.auto_delete(
            cache_root="FAKEDIR", keep_on_disk=1, handle_root=""
        )
    )
    assert not res
    _glob.assert_called_once_with("*/auto_delete.json")
    _rm.assert_not_called()


@patch("pathlib.Path.glob")
@patch("shutil.rmtree")
def test_auto_delete_all_no_remove(_rm, _glob):
    glob_list = [
        Mock(
            **{
                "read_text.return_value": json.dumps(
                    {"auto_delete": 1, "root": "mylib", "serial": 1000}
                )
            }
        ),
        Mock(
            **{
                "read_text.return_value": json.dumps(
                    {"auto_delete": 1, "root": "mylib1", "serial": 1001}
                )
            }
        ),
    ]
    _glob.return_value = glob_list

    res = list(
        res
        for res in store_payload.auto_delete(
            cache_root="FAKEDIR", keep_on_disk=1, handle_root=""
        )
    )
    assert not res
    _glob.assert_called_once_with("*/auto_delete.json")
    _rm.assert_not_called()
    for m in glob_list:
        m.read_text.assert_called()
        m.parent.assert_not_called()


@patch("pathlib.Path.glob")
@patch("shutil.rmtree")
def test_auto_delete_all_remove_one(_rm, _glob):
    glob_list = [
        Mock(
            **{
                "read_text.return_value": json.dumps(
                    {"auto_delete": 1, "root": "mylib", "serial": 1000}
                )
            }
        ),
        Mock(
            **{
                "read_text.return_value": json.dumps(
                    {"auto_delete": 1, "root": "mylib", "serial": 1001}
                )
            }
        ),
    ]
    _glob.return_value = glob_list

    res = list(
        res
        for res in store_payload.auto_delete(
            cache_root="FAKEDIR", keep_on_disk=1, handle_root=""
        )
    )
    assert res
    assert "auto remove expired" in res[0]

    _glob.assert_called_once_with("*/auto_delete.json")
    _rm.assert_called_once_with(glob_list[0].parent, ignore_errors=True)
    for m in glob_list:
        m.read_text.assert_called()
