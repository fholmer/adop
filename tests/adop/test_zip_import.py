import configparser
import os
import pathlib
from unittest.mock import ANY, mock_open, patch

import pytest

from adop import parse_config, zip_import


def _cf(conf_string, conf_dict={}):
    cf = configparser.ConfigParser()
    cf.read_string(conf_string)
    cf.read_dict(conf_dict)
    return cf


@patch("adop.parse_config.parse", return_value=_cf(""))
@patch("adop.zip_install.zip_install_sequences")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="")
def test_install_no_requires_data(_text, _exists, zip_install_sequences, local_config):

    with pytest.raises(SystemExit):
        zip_import.zip_import(
            "requires.ini", "adop.ini", ".", ["local"], "basedir", "", "", "", "", False
        )

    zip_install_sequences.client_install_zip_sequence.assert_not_called()

    local_config.assert_called()
    _text.assert_called_once_with()
    _exists.assert_called_once_with()


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        parse_config.default_config,
        {"remote:test": {"url": "", "token": "", "insecure": 0}},
    ),
)
@patch("adop.zip_install.zip_install_sequences")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.is_file", return_value=True)
@patch("pathlib.Path.read_text", return_value="[requires]")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_install_requires(
    _open, _text, _isfile, _exists, zip_install_sequences, local_config
):
    cf = local_config()
    file_as_uri = pathlib.Path("./fakezip.zip").absolute().as_uri()

    remote_data = [{"url": file_as_uri, "token": "", "insecure": False, "direct": True}]
    requires_data = {"mylib": "tag:0.0.1"}
    install_data = {
        "install_section": "install:basedir",
        "install_root": os.path.expanduser(cf.get("install:basedir", "install_root")),
        "cache_root": os.path.expanduser(cf.get("install:basedir", "cache_root")),
    }
    extra_data = {"mylib": {"Zip-Root": "fakezip-0.0.1"}}

    def gen(*args):
        yield {"root": "mylib"}
        yield "comment 1"

    zip_install_sequences.client_install_zip_sequence.side_effect = gen

    zip_import.zip_import(
        "requires.ini",
        "adop.ini",
        ".",
        ["test"],
        "basedir",
        "mylib",
        "./fakezip.zip",
        "0.0.1",
        "fakezip-0.0.1",
        False,
    )

    zip_install_sequences.client_install_zip_sequence.assert_called_once_with(
        install_data,
        ANY,
        remote_data,
        requires_data,
        extra_data,
    )

    local_config.assert_called()
    _text.assert_called_with()
    _isfile.assert_called_with()
    _exists.assert_called_once_with()
    _open.assert_called_once_with("w")
    handle = _open()
    handle.write.assert_called()
