import configparser
from unittest.mock import Mock, patch

import pytest

from adop import parse_config, restapi


@pytest.fixture()
def app():
    app = restapi.app
    app.config.update(
        {
            "TESTING": True,
            "deploy_root": "./FAKEDIR/auto",
            "cache_root": "./FAKEDIR/cache",
            "write_token": "NO",
            "read_token": "NO",
            "shared_lock": restapi.Lock(),
            "shared_progress_dict": {"test1": "test2"},
            "keep_on_disk": 0,
            "auto_fetch_event": Mock(),
        }
    )
    yield app


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def headers():
    return {"Token": "NO"}


def _cf(conf_list):
    cp = configparser.ConfigParser()
    if isinstance(conf_list, str):
        conf_list = [conf_list]

    for conf in conf_list:
        cp.read_string(conf)
    return cp


def test_invalid_token(client):
    responses = [
        client.get("/api/v1/test"),
        client.get("/api/v1/test", headers={"Token": ""}),
        client.get("/api/v1/test", headers={"Token": "TEST"}),
    ]
    for response in responses:
        assert b'"result":"Invalid token"' in response.data
        assert b'"result_code":4}' in response.data
        assert response.status_code == 401


def test_api_test(client, headers):
    response = client.get("/api/v1/test", headers=headers)
    assert b'"result":"It works"' in response.data
    assert b'"result_code":0}' in response.data
    assert response.status_code == 200


def test_api_progress(client, headers):
    response = client.get("/api/v1/progress", headers=headers)
    assert b'"test1":"test2"' in response.data
    assert response.status_code == 200


def test_api_progress_err(client, headers):
    client.application.config["shared_progress_dict"] = {None}
    response = client.get("/api/v1/progress", headers=headers)
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch("adop.deploy_state.get_config", return_value=({}, "", ""))
def test_api_state_internal_error(get_config, client, headers):
    response = client.get("/api/v1/state", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch(
    "adop.deploy_state.get_config", return_value=(configparser.ConfigParser(), "", "")
)
def test_api_state_empty(get_config, client, headers):
    response = client.get("/api/v1/state", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b"{}" in response.data
    assert response.status_code == 200


@patch(
    "adop.deploy_state.get_config",
    return_value=(_cf("[src_root]\nsource_hash=00"), "", ""),
)
def test_api_state(get_config, client, headers):
    response = client.get("/api/v1/state", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b'"src_root":"00"' in response.data
    assert response.status_code == 200


@patch(
    "adop.deploy_state.get_config",
    return_value=(_cf("[src_root]\nsource_hash=00"), "", ""),
)
def test_api_state_root_ismissing(get_config, client, headers):
    response = client.get("/api/v1/state/ismissing", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch("adop.deploy_state.get_config", return_value=(_cf("[src_root]"), "", ""))
def test_api_state_key_is_null(get_config, client, headers):
    response = client.get("/api/v1/state/src_root", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b'"src_root":null' in response.data
    assert response.status_code == 200


@patch(
    "adop.deploy_state.get_config",
    return_value=(_cf("[src_root]\nsource_hash=AA"), "", ""),
)
def test_api_state_root(get_config, client, headers):
    response = client.get("/api/v1/state/src_root", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b'"src_root":"AA"' in response.data
    assert response.status_code == 200


@patch(
    "adop.tags.get_config",
    return_value=(_cf("[src_root]\n0.0.1=AABBCC"), ""),
)
def test_api_tags_root(get_config, client, headers):
    response = client.get("/api/v1/tags/src_root", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/cache")
    assert b'"0.0.1":"AABBCC"' in response.data
    assert response.status_code == 200


@patch(
    "adop.tags.get_config",
    return_value=(_cf(""), ""),
)
def test_api_tags_root_is_missing(get_config, client, headers):
    response = client.get("/api/v1/tags/src_root", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/cache")
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch(
    "adop.tags.get_config",
    return_value=(_cf(""), ""),
)
def test_api_tags_tag_is_missing(get_config, client, headers):
    response = client.get("/api/v1/tags/src_root/tag", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/cache")
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch(
    "adop.tags.get_config",
    return_value=(_cf("[src_root]\n0.0.2=AABBCC"), ""),
)
def test_api_tags_root_tag(get_config, client, headers):
    response = client.get("/api/v1/tags/src_root/0.0.2", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/cache")
    assert b'"0.0.2":"AABBCC"' in response.data
    assert response.status_code == 200


@patch(
    "adop.tags.get_config",
    return_value=(_cf("[src_root]\n0.0.2=AABBCC"), ""),
)
def test_api_tags_root_tag_is_missing(get_config, client, headers):
    response = client.get("/api/v1/tags/src_root/0.0.1", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/cache")
    assert b"{}" in response.data
    assert response.status_code == 200


@patch(
    "adop.cache_state.get_dict",
    return_value={"AABBCC": {"sha256": "AABBCC"}},
)
def test_api_list_zip(get_dict, client, headers):
    response = client.get("/api/v1/list/zip/src_root", headers=headers)
    get_dict.assert_called_once_with("./FAKEDIR/cache", "src_root")
    assert b'"AABBCC":{"sha256":"AABBCC"}' in response.data
    assert response.status_code == 200


@patch("adop.cache_state.get_dict", side_effect=KeyError)
def test_api_list_zip_error(get_dict, client, headers):
    response = client.get("/api/v1/list/zip/src_root", headers=headers)
    get_dict.assert_called_once_with("./FAKEDIR/cache", "src_root")
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch("adop.restapi.send_file", return_value=b"00FAKEDATA")
@patch(
    "adop.deploy_state.get_config",
    return_value=(_cf("[dl]\nsource_file=FAKEFILE"), "", ""),
)
def test_api_download_zip_dl(get_config, send_file, client, headers):
    response = client.get("/api/v1/download/zip/dl", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    send_file.assert_called_once_with("FAKEFILE", as_attachment=True)
    assert response.data == b"00FAKEDATA"
    assert response.status_code == 200


@patch(
    "adop.deploy_state.get_config",
    return_value=(_cf("[dl]\nsource_file=FAKEFILE"), "", ""),
)
def test_api_download_zip_not_found(get_config, client, headers):
    response = client.get("/api/v1/download/zip/dl", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b'"result":"Internal Error FileNotFoundError' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch(
    "adop.deploy_state.get_config",
    return_value=(_cf("[dl]"), "", ""),
)
def test_api_download_zip_err(get_config, client, headers):
    response = client.get("/api/v1/download/zip/dl", headers=headers)
    get_config.assert_called_once_with("./FAKEDIR/auto")
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


def test_api_trigger_fetch(client, headers):
    response = client.get("/api/v1/trigger/fetch", headers=headers)
    restapi.app.config["auto_fetch_event"].set_fetch.assert_called_once_with()
    assert b'"result":"OK"' in response.data
    assert b'"result_code":0' in response.data
    assert response.status_code == 200


def test_api_trigger_fetch_not_available(client, headers):
    restapi.app.config["auto_fetch_event"] = None
    response = client.get("/api/v1/trigger/fetch", headers=headers)
    assert b'"result":"auto_fetch is not available"' in response.data
    assert b'"result_code":1' in response.data
    assert response.status_code == 503


def test_api_trigger_fetch_err(client, headers):
    restapi.app.config["auto_fetch_event"] = Mock([])
    response = client.get("/api/v1/trigger/fetch", headers=headers)
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


def test_api_trigger_fetch_root(client, headers):
    response = client.post("/api/v1/trigger/fetch/mylib", headers=headers)
    restapi.app.config["auto_fetch_event"].set_fetch.assert_called_once_with("mylib")
    assert b'"result":"OK"' in response.data
    assert b'"result_code":0' in response.data
    assert response.status_code == 200


def test_api_trigger_fetch_root_not_available(client, headers):
    restapi.app.config["auto_fetch_event"] = None
    response = client.post("/api/v1/trigger/fetch/mylib", headers=headers)
    assert b'"result":"auto_fetch is not available"' in response.data
    assert b'"result_code":1' in response.data
    assert response.status_code == 503


def test_api_trigger_fetch_root_err(client, headers):
    restapi.app.config["auto_fetch_event"] = Mock([])
    response = client.post("/api/v1/trigger/fetch/mylib", headers=headers)
    assert b'"result":"Internal Error' in response.data
    assert b'"result_code":5' in response.data
    assert response.status_code == 500


@patch("adop.restapi.handle_zip_as_stream")
def test_api_upload_zip(handle_zip, client, headers):
    def gen(root, store_data, unpack_data):
        yield f'{{"root": "{root}"}}'

    handle_zip.side_effect = gen

    response = client.post("/api/v1/upload/zip/mylib", headers=headers)

    handle_zip.assert_called_once_with("mylib", store_data=True, unpack_data=False)
    assert response.data == b'{"root": "mylib"}'
    assert response.status_code == 200


@patch("adop.restapi.handle_zip_as_stream")
def test_api_post_deploy_zip(handle_zip, client, headers):
    def gen(root, store_data, unpack_data):
        yield f'{{"root": "{root}"}}'

    handle_zip.side_effect = gen

    response = client.post("/api/v1/deploy/zip/mylib", headers=headers)

    handle_zip.assert_called_once_with("mylib", store_data=True, unpack_data=True)
    assert response.data == b'{"root": "mylib"}'
    assert response.status_code == 200


@patch("adop.restapi.handle_zip_as_stream")
def test_api_get_deploy_zip(handle_zip, client, headers):
    def gen(root, store_data, unpack_data):
        yield f'{{"root": "{root}"}}'

    handle_zip.side_effect = gen

    response = client.get("/api/v1/deploy/zip/mylib", headers=headers)

    handle_zip.assert_called_once_with("mylib", store_data=False, unpack_data=True)
    assert response.data == b'{"root": "mylib"}'
    assert response.status_code == 200


@patch("adop.zip_deploy_sequences.handle_zip_in_threadpool")
def test_handle_zip_as_stream(handle_zip, app):
    def gen(*args):
        yield {"root": "mylib"}
        yield {"progress": 0}
        yield "comment 1"
        yield {"progress": 30}
        yield "comment 2"
        yield {"progress": 60}
        yield {"root": "mylib", "result": "Success", "result_code": 0}

    handle_zip.side_effect = gen

    with app.test_request_context("/api/v1", method="GET", headers={}):
        res_list = list(
            i
            for i in restapi.handle_zip_as_stream(
                "mylib", store_data=True, unpack_data=True
            )
        )
    assert res_list == [
        "// comment 1\n",
        "// comment 2\n",
        '{"root": "mylib", "result": "Success", "result_code": 0}\n',
    ]


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        [
            parse_config.default_config,
            "[server]\ndebug = 1",
            "[server]\nwrite_token = NO",
            "[server]\nread_token = NO",
            "[autofetch]\non = 0",
        ]
    ),
)
@patch("adop.auto_fetch.run_in_background")
@patch("waitress.serve")
@patch("werkzeug.serving.run_simple")
def test_serve_app_run_simple(_werkzeug, _waitress, _autofetch, _config):
    restapi.serve(config="res", cwd=".", host=None, port=None)
    _werkzeug.assert_called_once_with(
        "127.0.0.1",
        8000,
        restapi.app,
        ssl_context=None,
        use_reloader=True,
        use_debugger=True,
        threaded=True,
    )
    _waitress.assert_not_called()
    _autofetch.assert_not_called()
    _config.assert_called_once()


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        [
            parse_config.default_config,
            "[server]\ndebug = 0",
            "[server]\nwrite_token = NO",
            "[server]\nread_token = NO",
            "[autofetch]\non = 0",
        ]
    ),
)
@patch("adop.auto_fetch.run_in_background")
@patch("waitress.serve")
@patch("werkzeug.serving.run_simple")
def test_serve_waitress_serve(_werkzeug, _waitress, _autofetch, _config):
    restapi.serve(config="res", cwd=".", host=None, port=None)
    _werkzeug.assert_not_called()
    _waitress.assert_called_once_with(restapi.app, host="127.0.0.1", port=8000)
    _autofetch.assert_not_called()
    _config.assert_called_once()


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        [
            parse_config.default_config,
            "[server]\ndebug = 0",
            "[server]\nssl_on = 1",
            "[server]\nwrite_token = NO",
            "[server]\nread_token = NO",
            "[autofetch]\non = 0",
        ]
    ),
)
@patch("adop.auto_fetch.run_in_background")
@patch("werkzeug.serving.run_simple")
@patch("adop.restapi.run_simple")
def test_serve_ssl_on(_runsimple, _werkzeug, _autofetch, _config):
    restapi.serve(config="res", cwd=".", host=None, port=None)
    _runsimple.assert_called_once_with(
        "127.0.0.1",
        8000,
        restapi.app,
        ssl_context=("", ""),
        use_reloader=False,
        use_debugger=False,
        threaded=True,
    )
    _werkzeug.assert_not_called()
    _autofetch.assert_not_called()
    _config.assert_called_once()


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        [
            parse_config.default_config,
            "[server]\ndebug = 0",
            "[server]\nwrite_token = NO",
            "[server]\nread_token = NO",
            "[autofetch]\non = 1",
        ]
    ),
)
@patch("adop.auto_fetch.run_in_background")
@patch("waitress.serve")
@patch("werkzeug.serving.run_simple")
def test_serve_app_run_waitress_and_autofetch(
    _werkzeug, _waitress, _autofetch, _config
):
    restapi.serve(config="res", cwd=".", host=None, port=None)
    _config.assert_called_once()
    _werkzeug.assert_not_called()
    _waitress.assert_called_once_with(restapi.app, host="127.0.0.1", port=8000)
    _autofetch.assert_called_once_with(restapi.app, _config())


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        [
            parse_config.default_config,
            "[server]\non = 0",
            "[server]\nwrite_token = NO",
            "[server]\nread_token = NO",
        ]
    ),
)
@patch("os.chdir")
def test_serve_chdir(_chdir: Mock, _config: Mock):
    restapi.serve(config="res", cwd=".", host=None, port=None)
    _chdir.assert_not_called()

    restapi.serve(config="res", cwd="", host=None, port=None)
    _chdir.assert_not_called()

    restapi.serve(config="res", cwd="./adop", host=None, port=None)
    _chdir.assert_called_once_with("./adop")


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        [parse_config.default_config, "[server]\non = 0", "[server]\ndebug = 1"]
    ),
)
@patch("os.chdir")
def test_serve_chdir_prevent_debug_and_rel_parent(_chdir: Mock, _config: Mock):
    with pytest.raises(SystemExit):
        restapi.serve(config="res", cwd="..", host=None, port=None)
    _chdir.assert_called_once_with("..")


@patch(
    "adop.parse_config.parse",
    return_value=_cf(["[server]\nwrite_token = "]),
)
def test_serve_no_token(_config: Mock):

    with pytest.raises(SystemExit):
        restapi.serve(config="res", cwd=".", host=None, port=None)

    _config.assert_called_once()
