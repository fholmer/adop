import configparser
import pathlib
from unittest.mock import ANY, patch

from adop import exceptions, zip_install_sequences


class assert_startswith(str):
    def __eq__(self, other):
        return other.startswith(self)

    def __repr__(self):
        return "<assert_startswith>"


@patch("adop.zip_install_sequences.downloaders")
@patch("adop.zip_install_sequences.verify_payload")
@patch("adop.zip_install_sequences.unpack_payload")
@patch("adop.zip_install_sequences.store_payload")
@patch("adop.zip_install_sequences.deploy_state")
def test_client_install_zip_sequence_no_requires(
    deploy_state, store_payload, unpack_payload, verify_payload, downloaders
):
    remote_data = [
        {
            "url": "fakeurl",
            "token": "NO",
        }
    ]
    requires_data = {
        # "mylib": ""
    }
    install_data = {
        "install_root": "./FAKEDIR/autolibs/auto",
        "cache_root": "./FAKEDIR/autolibs/cache",
        "install_section": "install:basedir",
    }
    extra_data = {}

    res = list(
        i
        for i in zip_install_sequences.client_install_zip_sequence(
            install_data=install_data,
            keep_on_disk=0,
            remote_data=remote_data,
            requires_data=requires_data,
            extra_data=extra_data,
        )
    )

    assert res == []

    assert not downloaders.mock_calls
    assert not verify_payload.mock_calls
    assert not unpack_payload.mock_calls
    assert not store_payload.mock_calls
    assert not deploy_state.mock_calls


@patch("adop.zip_install_sequences.downloaders")
@patch("adop.zip_install_sequences.verify_payload")
@patch("adop.zip_install_sequences.unpack_payload")
@patch("adop.zip_install_sequences.store_payload")
@patch("adop.zip_install_sequences.deploy_state")
def test_client_install_zip_sequence_mylib_sha256(
    deploy_state, store_payload, unpack_payload, verify_payload, downloaders
):
    mylib_sha256 = "5d9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5"
    mylib_tag = ""
    cache_file = pathlib.Path.cwd().joinpath("FAKEFILE")
    cf = configparser.ConfigParser()
    cf.read_dict({"mylib": {"source_hash": ""}})

    store_payload.find_file_from_headers.return_value = (
        cache_file,
        mylib_sha256,
        mylib_tag,
    )
    deploy_state.get_config.return_value = cf, ""
    unpack_payload.extract_root_dir_name.return_value = "mylib"

    remote_data = [
        {
            "url": "fakeurl",
            "token": "NO",
        }
    ]
    requires_data = {"mylib": "sha256:{mylib_sha256}"}
    install_data = {
        "install_root": "./FAKEDIR/autolibs/auto",
        "cache_root": "./FAKEDIR/autolibs/cache",
        "install_section": "install:basedir",
    }
    extra_data = {}

    res = list(
        i
        for i in zip_install_sequences.client_install_zip_sequence(
            install_data=install_data,
            keep_on_disk=0,
            remote_data=remote_data,
            requires_data=requires_data,
            extra_data=extra_data,
        )
    )

    def gen():
        yield {"root": "mylib"}
        yield assert_startswith("checking mylib=sha256:")
        yield assert_startswith("using cache_root:")
        yield assert_startswith("mylib already downloaded")
        yield assert_startswith("using install_root:")
        yield assert_startswith("mylib installation complete")
        yield {
            "result": "OK",
            "result_code": 0,
            "result_data": {
                "root": "mylib",
            },
        }

    assert res == list(gen())

    assert not downloaders.mock_calls
    verify_payload.verify_root.assert_not_called()
    unpack_payload.extract_root_dir_name.assert_not_called()
    unpack_payload.unpack.assert_called_once_with(
        cache_file, "./FAKEDIR/autolibs/auto", "mylib"
    )

    store_payload.find_file_from_headers.assert_called_once_with(
        "./FAKEDIR/autolibs/cache", "mylib", ANY
    )
    store_payload.auto_delete.assert_not_called()

    deploy_state.get_config.assert_called_once_with("./FAKEDIR/autolibs/auto")
    deploy_state.store.assert_called_once_with(
        "mylib", cache_file, mylib_sha256, "./FAKEDIR/autolibs/auto"
    )


@patch("adop.downloaders.api_download")
@patch("adop.zip_install_sequences.verify_payload")
@patch("adop.zip_install_sequences.unpack_payload")
@patch("adop.zip_install_sequences.store_payload")
@patch("adop.zip_install_sequences.deploy_state")
@patch("adop.tags.store")
def test_client_install_zip_sequence_mylib_tags(
    tags_store,
    deploy_state,
    store_payload,
    unpack_payload,
    verify_payload,
    api_download,
):
    cf = configparser.ConfigParser()
    cf.read_dict({"mylib": {"source_hash": ""}})
    deploy_state.get_config.return_value = cf, ""

    store_payload.find_file_from_headers.side_effect = exceptions.CacheNotFound("")

    mylib_sha256 = "5d9dbc5e554c6079bbcab9406a28a2b4bff27b580122b201469f69f527e331f5"
    mylib_tag = "0.1.2"
    cache_file = pathlib.Path.cwd().joinpath("FAKEFILE")

    def dl(*args):
        yield "DL"
        return (
            cache_file,
            mylib_sha256,
            mylib_tag,
        )

    api_download.side_effect = dl
    unpack_payload.extract_root_dir_name.return_value = "mylib"

    remote_data = [
        {
            "url": "fakeurl",
            "token": "NO",
        }
    ]
    requires_data = {"mylib": f"tag:{mylib_tag}"}
    install_data = {
        "install_root": "./FAKEDIR/autolibs/auto",
        "cache_root": "./FAKEDIR/autolibs/cache",
        "install_section": "install:basedir",
    }
    extra_data = {}

    res = list(
        i
        for i in zip_install_sequences.client_install_zip_sequence(
            install_data=install_data,
            keep_on_disk=0,
            remote_data=remote_data,
            requires_data=requires_data,
            extra_data=extra_data,
        )
    )

    def gen():
        yield {"root": "mylib"}
        yield assert_startswith("checking mylib=tag:")
        yield assert_startswith("using cache_root:")
        yield "DL"
        yield assert_startswith("update tag:")
        yield assert_startswith("using install_root:")
        yield assert_startswith("mylib installation complete")
        yield {
            "result": "OK",
            "result_code": 0,
            "result_data": {
                "root": "mylib",
            },
        }

    assert res == list(gen())

    api_download.assert_called_once_with(
        "./FAKEDIR/autolibs/cache", remote_data, "mylib", {"Zip-Tag": mylib_tag}
    )
    verify_payload.verify_root.assert_called_once_with("mylib", "mylib", "mylib")
    unpack_payload.extract_root_dir_name.assert_called_once_with(cache_file)
    unpack_payload.unpack.assert_called_once()
    store_payload.find_file_from_headers.assert_called_once()
    store_payload.auto_delete.assert_not_called()
    deploy_state.get_config.assert_called_once()
    deploy_state.store.assert_called_once_with(
        "mylib", cache_file, mylib_sha256, "./FAKEDIR/autolibs/auto"
    )
    tags_store.assert_called_once_with(
        "mylib", mylib_sha256, mylib_tag, "./FAKEDIR/autolibs/cache"
    )
