from unittest.mock import patch, sentinel

from adop import setup_rest_api


@patch("adop.restapi.serve")
def test_late_import(restapi):
    setup_rest_api.late_import(sentinel.RESTAPI)
    restapi.assert_called_once_with(sentinel.RESTAPI)
