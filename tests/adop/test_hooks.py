import pathlib
from unittest.mock import Mock, patch, sentinel

import pytest

from adop import exceptions, hooks


def test_transform_root():

    hooks.register.hook_entrypoint(
        "install:autolibs", "hook:transform-root", "builtins:basedir:add-as-prefix"
    )

    transformed = hooks.transform_root("install:autolibs", "mylib", {})

    basedir = pathlib.Path.cwd().stem
    assert f"{basedir}_mylib" == transformed


@patch("os.getenv", return_value=sentinel.env, spec=True)
def test_getenv(env):
    assert hooks.getenv("ENV") is sentinel.env
    env.assert_called_once_with("ENV", "")


def test_custom_hooks():
    with pytest.raises(exceptions.CommandFail):
        hooks.register.hook_entrypoint("section", "option", "custom:myhook")


@patch.dict("sys.modules", keyring=Mock(**{"get_password.return_value": sentinel.pw}))
def test_getkeyring():
    import keyring

    assert hooks.getkeyring("SRV:USR") is sentinel.pw
    keyring.get_password.assert_called_once_with("SRV", "USR")


def test_run_subprocess_error():
    with pytest.raises(exceptions.Fail):
        assert hooks.run_subprocess("run:./NOAPP", "")


def test_run_hook():
    fakehook = Mock(return_value=sentinel.HOOK)
    hooks.register.hook_callable("run")(fakehook)
    hooks.register.hook_entrypoint("hooks:mylib", "hook:testing", "run:some_hook")
    res = hooks.run_hook("hooks:mylib", "hook:testing", "mylib", {})
    assert res is sentinel.HOOK
