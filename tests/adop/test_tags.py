import configparser
import pathlib
from unittest.mock import mock_open, patch

import pytest

from adop import exceptions, tags


@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text", return_value="[sec1]key1=val1")
def test_get_config(_text, _mkdir, _exists):
    cache_root_path = str(pathlib.Path.cwd().joinpath("FAKEDIR"))

    config, config_file_path = tags.get_config(cache_root_path)

    assert config.sections() == ["sec1"]
    assert config_file_path.name == "tags.ini"
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _exists.assert_called_once_with()


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_store(_open, _mkdir, _exists):
    cache_root = str(pathlib.Path.cwd().joinpath("FAKEDIR"))

    assert tags.store("root_test", "FAKE_HASH", "0.0.1", cache_root) is None

    _open.assert_called_once_with(mode="w")
    handle = _open()
    handle.write.assert_called()


@patch("adop.tags.get_config")
def test_find_shasum_from_headers(_config):
    _cf = configparser.ConfigParser()
    _cf.read_dict({"mylib": {"0.0.1": "AABBCC"}})
    _config.return_value = _cf, ""
    shasum = tags.find_shasum_from_headers(
        ".FAKEDIR/cache", "mylib", {"Zip-Tag": "0.0.1"}
    )
    assert shasum == "AABBCC"


@patch("adop.tags.get_config")
def test_find_shasum_from_headers_no_tag(_config):
    _config.return_value = configparser.ConfigParser(), ""
    with pytest.raises(exceptions.Fail) as err:
        tags.find_shasum_from_headers(".FAKEDIR/cache", "mylib", {})
    assert "Zip-Tag is missing" in str(err)


@patch("adop.tags.get_config")
def test_find_shasum_from_headers_wrong_tag(_config):
    _config.return_value = configparser.ConfigParser(), ""
    with pytest.raises(exceptions.Fail) as err:
        tags.find_shasum_from_headers(
            ".FAKEDIR/cache", "mylib", {"Zip-Tag": "AA BB CC"}
        )
    assert "Zip-Tag is not a valid tag" in str(err)


@patch("adop.tags.get_config")
def test_find_shasum_from_headers_wrong_config(_config):
    _cf = configparser.ConfigParser()
    _config.return_value = _cf, ""

    with pytest.raises(exceptions.CacheNotFound) as err:
        tags.find_shasum_from_headers(".FAKEDIR/cache", "mylib", {"Zip-Tag": "2022"})
    assert "Zip-Tag not found" in str(err)

    _cf.read_dict({"mylib": {}})
    with pytest.raises(exceptions.CacheNotFound) as err:
        tags.find_shasum_from_headers(".FAKEDIR/cache", "mylib", {"Zip-Tag": "2022"})
    assert "Zip-Tag not found" in str(err)

    _cf.read_dict({"mylib": {"2022": ""}})
    with pytest.raises(exceptions.CacheNotFound) as err:
        tags.find_shasum_from_headers(".FAKEDIR/cache", "mylib", {"Zip-Tag": "2022"})
    assert "Zip-Tag not found" in str(err)

    _cf.read_dict({"mylib": {"2022": "FOUND"}})
    res = tags.find_shasum_from_headers(".FAKEDIR/cache", "mylib", {"Zip-Tag": "2022"})
    assert res == "FOUND"
