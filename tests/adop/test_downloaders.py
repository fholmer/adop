from unittest.mock import Mock, mock_open, patch
from urllib import error, response

import pytest

from adop import downloaders, exceptions


def _make_urlopen_response(data, headers):
    fp = mock_open(read_data=data)()
    fp.closed = False
    return response.addinfo(fp, headers)


@patch("adop.store_payload.store", return_value=(Mock(), "CI"))
@patch("adop.verify_payload.verify_content")
@patch("adop.downloaders.simple_api_downloader")
def test_api_download_calls_simple_dl(simple_dl, verify, store):
    def gen(*args, **kwargs):
        yield ""
        return b"ABC", 3

    simple_dl.side_effect = gen

    remote_data = {"url": "", "token": "", "insecure": 0}
    res = list(
        i
        for i in downloaders.api_download("./FAKEDIR/CACHE", [remote_data], "mylib", {})
    )
    assert res
    simple_dl.assert_called_once_with(remote_data, "mylib", {})
    verify.assert_called_once()
    store.assert_called_once()


@patch("adop.store_payload.store", return_value=(Mock(), "CI"))
@patch("adop.verify_payload.verify_content")
@patch("adop.downloaders.simple_api_downloader")
def test_api_download_fail_as_commandfail(simple_dl, verify, store):
    def gen(*args, **kwargs):
        raise exceptions.Fail("TEST")

    simple_dl.side_effect = gen

    remote_data = {"url": "", "token": "", "insecure": 0}
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in downloaders.api_download(
                "./FAKEDIR/CACHE", [remote_data], "mylib", {}
            )
        )
    simple_dl.assert_called_once()


@patch("adop.store_payload.store", return_value=(Mock(), "CI"))
@patch("adop.verify_payload.verify_content")
@patch("adop.downloaders.simple_api_downloader")
def test_api_download_HTTPError(simple_dl, verify, store):
    simple_dl.side_effect = error.HTTPError("", "", "", "", Mock())

    remote_data = {"url": "", "token": "", "insecure": 0}
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in downloaders.api_download(
                "./FAKEDIR/CACHE", [remote_data], "mylib", {}
            )
        )
    simple_dl.assert_called_once()


@patch("adop.store_payload.store", return_value=(Mock(), "CI"))
@patch("adop.verify_payload.verify_content")
@patch("adop.downloaders.simple_api_downloader")
def test_api_download_URLError(simple_dl, verify, store):
    simple_dl.side_effect = error.URLError("", "")

    remote_data = {"url": "", "token": "", "insecure": 0}
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in downloaders.api_download(
                "./FAKEDIR/CACHE", [remote_data], "mylib", {}
            )
        )
    simple_dl.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(
        b"AAA1BB2CCF",
        {"Content-Type": "application/zip", "Content-Length": f"{1024*1024*10 + 1}"},
    ),
)
def test_simple_api_downloader(_urlopen):
    remote_data = {
        "url": "uri://uri.local/",
        "token": "",
        "insecure": 1,
        "direct": False,
    }
    res = list(i for i in downloaders.simple_api_downloader(remote_data, "mylib", {}))
    assert res
    _urlopen.assert_called_once()


@patch("urllib.request.urlopen")
@patch("pathlib.Path.is_file", return_value=True)
@patch("pathlib.Path.read_bytes", return_value=b"AAA1BB2CCF")
def test_simple_api_downloader_local_file(_read_bytes, _isfile, _urlopen):
    remote_data = {
        "url": "file:///FAKEDIR/FAKEZIP.zip",
        "token": "",
        "insecure": 1,
        "direct": False,
    }

    def yield_return_value():
        res = yield from downloaders.simple_api_downloader(remote_data, "mylib", {})
        yield res

    res = list(i for i in yield_return_value())
    assert res
    assert res[-1] == (b"AAA1BB2CCF", 10)
    _read_bytes.assert_called_once()
    _isfile.assert_called_once()
    _urlopen.assert_not_called()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(
        b"AAA1BB2CCF",
        {
            "Content-Type": "application/x-zip-compressed",
            "Content-Length": f"{1024*1024*10 + 1}",
        },
    ),
)
def test_simple_api_downloader_x_zip_compressed(_urlopen):
    remote_data = {
        "url": "uri://uri.local/",
        "token": "",
        "insecure": 1,
        "direct": False,
    }
    res = list(i for i in downloaders.simple_api_downloader(remote_data, "mylib", {}))
    assert res
    _urlopen.assert_called_once()
