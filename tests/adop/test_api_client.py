from unittest.mock import Mock, mock_open, patch, sentinel
from urllib import error, response

import pytest

from adop import exceptions, api_client


def _make_urlopen_response(data, headers):
    fp = mock_open(read_data=data)()
    fp.closed = False
    return response.addinfo(fp, headers)


@patch("adop.api_client.simple_api_client", spec=True)
def test_api_upload_calls_simple_api_client(simple_client: Mock):
    remote_data = {"url": "", "token": "", "insecure": 0}
    res = list(i for i in api_client.api_client("uri://uri.local/", remote_data, {}))
    assert res == []
    simple_client.assert_called_once_with("uri://uri.local/", remote_data, {})


@patch("adop.api_client.simple_api_client", spec=True, side_effect=exceptions.Fail)
def test_api_upload_calls_simple_api_client_fail(simple_client: Mock):
    with pytest.raises(exceptions.CommandFail):
        list(i for i in api_client.api_client("test", {}, {}))
    simple_client.assert_called_once()


@patch(
    "adop.api_client.simple_api_client",
    spec=True,
    side_effect=error.HTTPError("", "", "", "", Mock()),
)
def test_api_upload_calls_simple_api_client_HTTPError(simple_client: Mock):
    with pytest.raises(exceptions.CommandFail):
        list(i for i in api_client.api_client("test", {}, {}))
    simple_client.assert_called_once()


@patch(
    "adop.api_client.simple_api_client", spec=True, side_effect=error.URLError("", "")
)
def test_api_upload_calls_simple_api_client_URLError(simple_client: Mock):
    with pytest.raises(exceptions.CommandFail):
        list(i for i in api_client.api_client("test", {"url": ""}, {}))
    simple_client.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(
        b'//line1\n//line2\n//line3\n{"result_code":0}', {}
    ),
)
def test_simple_api_client(_urlopen: Mock):
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    res = list(i for i in api_client.simple_api_client("test", remote_data, {}))
    assert res
    assert "      line1" in res
    assert "      line2" in res
    assert "      line3" in res
    _urlopen.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(b'//line1\n{"result_code":1}', {}),
)
def test_simple_api_client_fail(_urlopen: Mock):
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    with pytest.raises(exceptions.CommandFail):
        list(i for i in api_client.simple_api_client("test", remote_data, {}))
    _urlopen.assert_called_once()
