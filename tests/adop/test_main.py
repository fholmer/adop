import argparse
from unittest.mock import Mock, patch

import pytest

from adop import __main__


@patch("argparse.ArgumentParser.parse_args")
def test_func_none(parse_args):
    parse_args.return_value = argparse.Namespace(func=None)

    __main__.main()

    parse_args.assert_called_once_with()


@patch("argparse.ArgumentParser.parse_args")
def test_func_callback(parse_args):
    func = Mock()
    parse_args.return_value = argparse.Namespace(func=func)

    __main__.main()

    parse_args.assert_called_once_with()
    func.assert_called_once_with()


@patch("argparse.ArgumentParser.parse_args")
def test_func_callback_aborted_by_user(parse_args):
    func = Mock(side_effect=KeyboardInterrupt)
    parse_args.return_value = argparse.Namespace(func=func)

    with pytest.raises(SystemExit) as err:
        __main__.main()

    assert "Aborted by user" in err.value.code


@patch("argparse.ArgumentParser.parse_args")
def test_func_callback_command_fail(parse_args):
    func = Mock(side_effect=__main__.exceptions.CommandFail("FAIL"))
    parse_args.return_value = argparse.Namespace(func=func)

    with pytest.raises(SystemExit) as err:
        __main__.main()

    assert "FAIL" in err.value.code
