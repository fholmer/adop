import json
import pathlib
from unittest.mock import patch

from adop import cache_state

METADATA = {
    "root": "mylib",
    "root_from_url": "mylib",
    "ignore this": "extra field",
    "sha256": "AABBCC",
    "tag": "0.1.2",
    "serial": 1666198148.5022213,
}


@patch("pathlib.Path.glob", return_value=[pathlib.Path(".mylib/AABBCC/metadata.json")])
@patch("pathlib.Path.read_text", return_value=json.dumps(METADATA))
def test_get_dict(read_text, glob):
    res = cache_state.get_dict("./FAKEDIR/CACHE", "mylib")

    assert res == {
        "mylib": {
            METADATA.get("sha256"): {
                "sha256": METADATA.get("sha256"),
                "tag": METADATA.get("tag"),
                "serial": METADATA.get("serial"),
            }
        }
    }

    read_text.assert_called_once()
    glob.assert_called_once()
