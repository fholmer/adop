import pathlib
from unittest.mock import ANY, Mock, patch

import pytest

from adop import restapi, zip_deploy_sequences


class assert_startswith(str):
    def __eq__(self, other):
        return other.startswith(self)

    def __repr__(self):
        return "<assert_startswith>"


@pytest.fixture()
def app():
    app = restapi.app
    app.config.update(
        {
            "TESTING": True,
            "deploy_root": "./FAKEDIR/auto",
            "cache_root": "./FAKEDIR/cache",
            "write_token": "NO",
            "read_token": "NO",
            "shared_lock": restapi.Lock(),
            "shared_progress_dict": {"test1": "test2"},
            "keep_on_disk": 1,
            "auto_fetch_event": Mock(),
        }
    )
    yield app


@patch("adop.zip_deploy_sequences.verify_payload")
@patch("adop.zip_deploy_sequences.unpack_payload")
@patch("adop.zip_deploy_sequences.store_payload")
@patch("adop.zip_deploy_sequences.deploy_state")
def test_deploy_zip_sequence_internal_err(
    deploy_state, store_payload, unpack_payload, verify_payload, app
):

    res = list(
        i
        for i in zip_deploy_sequences.deploy_zip_sequence(None, None, True, None, None)
    )

    assert res
    assert res[-1]["result"] == "Fail"
    assert res[-1]["result_code"] == 1


@patch("adop.zip_deploy_sequences.verify_payload")
@patch("adop.zip_deploy_sequences.unpack_payload")
@patch("adop.zip_deploy_sequences.store_payload")
@patch("adop.zip_deploy_sequences.deploy_state")
def test_deploy_zip_sequence_internal_fail(
    deploy_state, store_payload, unpack_payload, verify_payload, app
):

    res = list(
        i
        for i in zip_deploy_sequences.deploy_zip_sequence(None, None, None, None, None)
    )

    assert res
    assert res[-1]["result"] == "HTTP url <root> not found"
    assert res[-1]["result_code"] == 1


@patch("adop.zip_deploy_sequences.verify_payload")
@patch("adop.zip_deploy_sequences.unpack_payload")
@patch("adop.zip_deploy_sequences.store_payload")
@patch("adop.zip_deploy_sequences.deploy_state")
def test_deploy_zip_sequence_no_root(
    deploy_state, store_payload, unpack_payload, verify_payload, app
):

    with app.test_request_context():
        res = list(
            i
            for i in zip_deploy_sequences.deploy_zip_sequence(
                app, restapi.request, None, store_data=True, unpack_data=True
            )
        )

    assert res
    assert {"root": None} in res
    assert "HTTP url <root> not found" in res[-1]["result"]
    assert res[-1]["result_code"] == 1

    assert not verify_payload.mock_calls
    assert not unpack_payload.mock_calls
    assert not store_payload.mock_calls
    assert not deploy_state.mock_calls


@patch("pathlib.Path.stat")
@patch("adop.zip_deploy_sequences.verify_payload")
@patch("adop.zip_deploy_sequences.unpack_payload")
@patch("adop.zip_deploy_sequences.store_payload")
@patch("adop.zip_deploy_sequences.deploy_state")
@patch("adop.tags.store")
def test_deploy_zip_sequence_post_zip(
    tags_store, deploy_state, store_payload, unpack_payload, verify_payload, stat, app
):
    cache_file = pathlib.Path.cwd().joinpath("FAKEFILE")
    store_payload.store.return_value = cache_file, "FAKESUM"
    unpack_payload.extract_root_dir_name.return_value = "mylib"

    with app.test_request_context(
        "/api/v1", method="POST", data=b"DATA", headers={"Zip-Tag": "TAG"}
    ):
        res = list(
            i
            for i in zip_deploy_sequences.deploy_zip_sequence(
                app, restapi.request, "mylib", store_data=True, unpack_data=True
            )
        )

    assert res
    assert {"root": "mylib"} in res
    assert {"root": "mylib", "result": "Success", "result_code": 0} in res

    ASSERT_DATA_LEN = len(b"DATA")

    store_payload.store.assert_called_once_with(
        b"DATA", "./FAKEDIR/cache", "mylib", "TAG", ANY
    )
    store_payload.auto_delete.assert_called_once_with("./FAKEDIR/cache", 1, "mylib")
    store_payload.find_file_from_headers.assert_not_called()
    verify_payload.verify_content.assert_called_once_with(
        ASSERT_DATA_LEN, ASSERT_DATA_LEN, ANY
    )
    verify_payload.verify_root.assert_called_once_with("mylib", "mylib", "")
    unpack_payload.extract_root_dir_name.assert_called_once_with(cache_file)
    unpack_payload.unpack.assert_called_once_with(cache_file, "./FAKEDIR/auto", "mylib")
    deploy_state.store.assert_called_once_with(
        "mylib", cache_file, "FAKESUM", "./FAKEDIR/auto"
    )
    tags_store.assert_called_once_with("mylib", "FAKESUM", "TAG", "./FAKEDIR/cache")


@patch("pathlib.Path.stat")
@patch("adop.zip_deploy_sequences.verify_payload")
@patch("adop.zip_deploy_sequences.unpack_payload")
@patch("adop.zip_deploy_sequences.store_payload")
@patch("adop.zip_deploy_sequences.deploy_state")
def test_deploy_zip_sequence_get_zip(
    deploy_state, store_payload, unpack_payload, verify_payload, stat, app
):
    cache_file = pathlib.Path.cwd().joinpath("FAKEFILE")
    store_payload.find_file_from_headers.return_value = cache_file, "FAKESUM", "FAKETAG"
    unpack_payload.extract_root_dir_name.return_value = "mylib"

    with app.test_request_context("/api/v1", method="GET", headers={}):
        res = list(
            i
            for i in zip_deploy_sequences.deploy_zip_sequence(
                app, restapi.request, "mylib", store_data=False, unpack_data=False
            )
        )

    assert res
    assert {"root": "mylib"} in res
    assert {"root": "mylib", "result": "Success", "result_code": 0} in res

    ASSERT_HEADERS = {"Host": "localhost"}

    store_payload.store.assert_not_called()
    store_payload.auto_delete.assert_not_called()
    store_payload.find_file_from_headers.assert_called_once_with(
        "./FAKEDIR/cache", "mylib", ASSERT_HEADERS
    )

    verify_payload.verify_content.assert_not_called()
    verify_payload.verify_root.assert_called_once_with("mylib", "mylib", "")

    unpack_payload.extract_root_dir_name.assert_called_once_with(cache_file)
    unpack_payload.unpack.assert_not_called()

    deploy_state.store.assert_not_called()


@patch("adop.zip_deploy_sequences.handle_zip")
def test_handle_zip_in_threadpool(handle_zip, app):
    def gen(*args, **kwargs):
        yield {"root": "mylib"}
        yield {"progress": 0}
        yield "comment 1"
        yield {"root": "mylib", "result": "Success", "result_code": 0}

    handle_zip.side_effect = gen

    res_list = list(
        i
        for i in zip_deploy_sequences.handle_zip_in_threadpool(
            app, restapi.request, "mylib", store_data=True, unpack_data=True
        )
    )
    assert res_list == list(gen())


@patch("adop.zip_deploy_sequences.handle_zip")
def test_handle_zip_in_threadpool_exception(handle_zip, app):
    def gen(*args, **kwargs):
        yield "line 1"
        yield "line 2"
        raise ValueError

    handle_zip.side_effect = gen

    res_list = list(
        i
        for i in zip_deploy_sequences.handle_zip_in_threadpool(
            app, restapi.request, "", store_data=True, unpack_data=True
        )
    )

    assert res_list == ["line 1", "line 2", ANY]
    assert isinstance(res_list[-1], ValueError)


@patch("adop.zip_deploy_sequences.deploy_zip_sequence")
def test_handle_zip_progress_dict(deploy_zip_sequence, app):
    def gen(*args):
        yield {"root": "mylib"}
        yield {"progress": 0}
        yield "comment 1"
        yield {"progress": 30}
        yield "comment 2"
        yield {"progress": 60}
        yield {"root": "mylib", "result": "Success", "result_code": 0}

    deploy_zip_sequence.side_effect = gen

    with app.test_request_context("/api/v1", method="GET", headers={}):
        res_list = list(
            i
            for i in zip_deploy_sequences.handle_zip(
                app, restapi.request, "mylib", True, True
            )
        )

    assert res_list == list(gen())

    progress = app.config["shared_progress_dict"]
    assert progress
    assert progress["mylib"]
    assert progress["mylib"]["log"] == ["comment 1", "comment 2"]
    assert progress["mylib"]["progress"] == 60
    assert progress["mylib"]["result"] == "Success"
    assert progress["mylib"]["result_code"] == 0
