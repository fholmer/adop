from unittest.mock import Mock

import pytest

from adop import exceptions, verify_payload


def test_verify_content_pass():

    m = Mock()
    gen = verify_payload.verify_content(
        header_content_len=50, received_data_len=50, file_len=50
    )
    for i in gen:
        m(i)
    m.assert_called_once()

    m = Mock()
    for i in verify_payload.verify_content(0, 0, 0):
        m(i)
    m.assert_called_once()


def test_verify_content_value_error():

    with pytest.raises(exceptions.Fail):
        m = Mock()
        for i in verify_payload.verify_content(50, 60, 50):
            m(i)
    m.assert_called_once()

    with pytest.raises(exceptions.Fail):
        m = Mock()
        for i in verify_payload.verify_content(60, 50, 50):
            m(i)
    m.assert_called_once()


def test_verify_root_pass():

    m = Mock()
    for i in verify_payload.verify_root("TEST", "TEST", ""):
        m(i)
    m.assert_called_once()

    m = Mock()
    for i in verify_payload.verify_root("T_E_S_T", "t_e_s_t", ""):
        m(i)
    m.assert_called_once()

    m = Mock()
    for i in verify_payload.verify_root("T_E_S_T", "", "t_e_s_t"):
        m(i)
    m.assert_called_once()


def test_verify_root_value_error():

    with pytest.raises(exceptions.Fail):
        m = Mock()
        gen = verify_payload.verify_root(
            root_dir_name="", root_from_url="", root_from_header=""
        )
        for i in gen:
            m(i)
        m.assert_called_once()

    with pytest.raises(exceptions.Fail):
        m = Mock()
        for i in verify_payload.verify_root("TES", "TEST", ""):
            m(i)
        m.assert_called_once()

    with pytest.raises(exceptions.Fail):
        m = Mock()
        for i in verify_payload.verify_root("T_E_S_T", "", ""):
            m(i)
        m.assert_called_once()


def test_verify_safe_basename_pass():
    assert verify_payload.verify_safe_basename("eéêèóõöò~£ÆØÅæøå'")
    assert verify_payload.verify_safe_basename(
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_ ."
    )


def test_verify_safe_basename_error():
    assert not verify_payload.verify_safe_basename("")
    assert not verify_payload.verify_safe_basename("COM1")
    assert not verify_payload.verify_safe_basename("c:\\abspath")
    assert not verify_payload.verify_safe_basename("/abspath")
    assert not verify_payload.verify_safe_basename("relfile > newfile")
    assert not verify_payload.verify_safe_basename("$param")
    assert not verify_payload.verify_safe_basename("%param%")
