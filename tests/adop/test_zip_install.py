import configparser
import os
from unittest.mock import ANY, patch, sentinel

import pytest

from adop import exceptions, parse_config, zip_install


def _cf(conf_string, conf_dict={}):
    cf = configparser.ConfigParser()
    cf.read_string(conf_string)
    cf.read_dict(conf_dict)
    return cf


def test_compare_version():

    # TODO: Improve this
    assert zip_install.compare_version("0.0.1", "0.0.1")


@patch("adop.parse_config.parse", return_value=_cf(""))
@patch("adop.zip_install.zip_install_sequences")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="")
def test_install_no_requires_data(_text, _exists, zip_install_sequences, local_config):

    with pytest.raises(exceptions.CommandFail):
        zip_install.install("requires.ini", "adop.ini", ".", ["local"], "basedir")

    zip_install_sequences.client_install_zip_sequence.assert_not_called()

    local_config.assert_called()
    _text.assert_called_once_with()
    _exists.assert_called_once_with()


@patch("adop.parse_config.parse", return_value=_cf(""))
@patch("adop.zip_install.zip_install_sequences")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[requires]")
def test_install_no_remote_config_data(
    _text, _exists, zip_install_sequences, local_config
):

    with pytest.raises(exceptions.CommandFail):
        zip_install.install("requires.ini", "adop.ini", ".", ["local"], "basedir")

    zip_install_sequences.client_install_zip_sequence.assert_not_called()

    local_config.assert_called()
    _text.assert_called_once_with()
    _exists.assert_called_once_with()


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        "", {"remote:notmissing": {"url": "", "token": ""}, "install:missing": {}}
    ),
)
@patch("adop.zip_install.zip_install_sequences")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[requires]\nmylib=tag:missing")
def test_install_no_install_section_config_data(
    _text, _exists, zip_install_sequences, local_config
):

    with pytest.raises(exceptions.CommandFail) as err:
        zip_install.install("requires.ini", "adop.ini", ".", ["notmissing"], "missing")

    assert "install:missing" in str(err)
    assert "No option " in str(err)

    zip_install_sequences.client_install_zip_sequence.assert_not_called()

    local_config.assert_called()
    _text.assert_called_once_with()
    _exists.assert_called_once_with()


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        "",
        {
            "install:notmissing": {"install_root": "", "cache_root": ""},
            "remote:missing": {},
        },
    ),
)
@patch("adop.zip_install.zip_install_sequences")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[requires]\nmylib=tag:missing")
def test_install_no_remote_section_config_data(
    _text, _exists, zip_install_sequences, local_config
):

    with pytest.raises(exceptions.CommandFail) as err:
        zip_install.install("requires.ini", "adop.ini", ".", ["missing"], "notmissing")

    assert "remote:missing" in str(err)
    assert "No option " in str(err)

    zip_install_sequences.client_install_zip_sequence.assert_not_called()

    local_config.assert_called()
    _text.assert_called_once_with()
    _exists.assert_called_once_with()


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        parse_config.default_config,
        {"remote:test": {"url": "", "token": "", "insecure": 0}},
    ),
)
@patch("adop.zip_install.zip_install_sequences")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[requires]\n[tool_requires]\nadop=0.0.1")
def test_install_requires(_text, _exists, zip_install_sequences, local_config):
    cf = local_config()

    remote_data = [{"url": "", "token": "", "insecure": False, "direct": False}]
    requires_data = {}
    install_data = {
        "install_section": "install:basedir",
        "install_root": os.path.expanduser(cf.get("install:basedir", "install_root")),
        "cache_root": os.path.expanduser(cf.get("install:basedir", "cache_root")),
    }
    extra_data = {}

    def gen(*args):
        yield {"root": "mylib"}
        yield "comment 1"

    zip_install_sequences.client_install_zip_sequence.side_effect = gen

    zip_install.install("requires.ini", "adop.ini", ".", ["test"], "basedir")

    zip_install_sequences.client_install_zip_sequence.assert_called_once_with(
        install_data,
        ANY,
        remote_data,
        requires_data,
        extra_data,
    )

    local_config.assert_called()
    _text.assert_called_once_with()
    _exists.assert_called_once_with()


@patch("adop.zip_install_sequences.client_install_zip_sequence")
def test_Installer_install(zip_sequence, capfd):
    def gen(*args):
        yield {"root": "mylib"}
        yield "comment 1"
        yield {"result": "OK", "result_code": 0}

    zip_sequence.side_effect = gen
    installer = zip_install.Installer(
        _cf("", {"auto_delete": {"on": "1", "keep_on_disk": "0"}})
    )

    installer.install(
        sentinel.install, sentinel.remote, sentinel.require, sentinel.extra
    )

    zip_sequence.assert_called_once_with(
        sentinel.install, 0, sentinel.remote, sentinel.require, sentinel.extra
    )
    out = capfd.readouterr().out
    assert "Requires: mylib" in out
    assert "comment 1" in out
    assert "OK" in out


@patch("adop.zip_install_sequences.client_install_zip_sequence")
def test_Installer_install_commandfail(zip_sequence, capfd):
    def gen(*args):
        yield {"root": "mylib"}
        raise exceptions.CommandFail("ERR!")

    zip_sequence.side_effect = gen
    installer = zip_install.Installer(_cf("", {"auto_delete": {"on": "0"}}))

    with pytest.raises(exceptions.CommandFail) as err:
        installer.install(
            sentinel.install, sentinel.remote, sentinel.require, sentinel.extra
        )
    assert "ERR!" in str(err.value)

    zip_sequence.assert_called_once_with(
        sentinel.install, 0, sentinel.remote, sentinel.require, sentinel.extra
    )
    assert "ERR" in capfd.readouterr().out
