import pathlib
from unittest.mock import mock_open, patch

from adop import deploy_state


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text")
def test_get_config(_text, _mkdir, _exists):
    _exists.return_value = True
    _text.return_value = "[sec1]key1=val1"
    deploy_root_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    config, unpack_root, config_file_path = deploy_state.get_config(
        str(deploy_root_path)
    )

    assert config.sections() == ["sec1"]
    assert deploy_root_path == unpack_root
    assert config_file_path.name == "autodeploystate.ini"
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _exists.assert_called_once_with()


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_store(_open, _mkdir, _exists):
    _exists.return_value = False
    deploy_root_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    root_dir_name = "root_test"
    cache_file = pathlib.Path("FAKEFILE")
    cache_hash = "FAKE_HASH"
    deploy_root = str(deploy_root_path)

    assert (
        deploy_state.store(root_dir_name, cache_file, cache_hash, deploy_root) is None
    )

    _open.assert_called_once_with(mode="w")
    handle = _open()
    handle.write.assert_called()
