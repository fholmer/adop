import configparser
from unittest.mock import ANY, Mock, mock_open, patch
from urllib import request, response

import pytest

from adop import auto_fetch


def _make_urlopen_response(data, headers):
    fp = mock_open(read_data=data)()
    fp.closed = False
    return response.addinfo(fp, headers)


def test_unpack_headers():
    assert auto_fetch.unpack_headers("k: v") == {"k": "v"}
    assert auto_fetch.unpack_headers("aa:bb, ccc :  ddd") == {"aa": "bb", "ccc": "ddd"}

    with pytest.raises(ValueError):
        assert auto_fetch.unpack_headers("")

    with pytest.raises(ValueError):
        assert auto_fetch.unpack_headers("k:v,")


@patch("urllib.request.urlopen")
def test_RequestInterface_init(_urlopen):
    r = request.Request(url="uri:test", headers={})
    ri = auto_fetch.RequestInterface(
        {"Zip-Root": ""}, url=r, remote_addr="remote_addr", remote_user="remote_user"
    )

    assert ri.headers == {"Zip-Root": ""}
    assert ri.remote_addr == "remote_addr"
    assert ri.remote_user == "remote_user"
    _urlopen.assert_not_called()


@patch("urllib.request.urlopen")
def test_RequestInterface_get_data(_urlopen):
    _request = request.Request(url="uri:test", headers={})
    _urlopen.return_value = _make_urlopen_response(b"AAA1BB2CCF", {})

    ri = auto_fetch.RequestInterface({}, _request, "", "")
    assert ri.get_data() == b"AAA1BB2CCF"
    _urlopen.assert_called_once_with(_request)


@patch("adop.zip_deploy_sequences.handle_zip")
@patch("adop.auto_fetch_state.State")
@patch("urllib.request.urlopen")
def test_Auto_Fetch_Thread_check_fetch_updates(_urlopen, _state, _handle_zip):
    _urlopen.return_value = _make_urlopen_response(b"AAA1BB2CCF", {})
    _handle_zip.return_value = iter(["test", {"result_code": 0}])

    af = auto_fetch.Auto_Fetch_Thread()
    af.app = None
    af.config = configparser.ConfigParser()
    sources_config = configparser.ConfigParser()
    sources_config.read_dict(
        {
            "autofetch": {"sources": "src1"},
            "autofetch:src1": {
                "root": "src1",
                "check_url": "uri:check",
                "payload_url": "uri:content",
            },
        }
    )
    af.fetch_updates(
        cache_root="FAKEROOT", sources_config=sources_config, filter_root=None
    )

    _state.assert_called_with("FAKEROOT")
    _state().update.assert_called_once_with("autofetch:src1", ANY)
    _state().store.assert_called_once_with()
    _urlopen.assert_called_once_with(ANY)
    _handle_zip.assert_called_once_with(
        af.app, ANY, "src1", store_data=True, unpack_data=True
    )


def test_interrupt_fetch():
    intr = auto_fetch.Interrupt()
    intr.set_fetch()
    assert intr.is_set()
    assert intr.fetch
    assert not intr.exit


def test_interrupt_exit():
    intr = auto_fetch.Interrupt()
    intr.set_exit()
    assert intr.is_set()
    assert not intr.fetch
    assert intr.exit


def test_interrupt_clear():
    intr = auto_fetch.Interrupt()
    intr.set_exit()
    assert intr.is_set()
    assert intr.exit
    intr.clear()
    assert not intr.is_set()
    assert not intr.exit


@patch("adop.auto_fetch_state.State")
@patch("atexit.register")
def test_auto_fetch_thread_run(_atexit: Mock, _state: Mock):
    app = Mock()
    app.config = {"auto_fetch_event": None}
    config = configparser.ConfigParser()
    config.read_dict(
        {
            "server": {"cache_root": "FAKEROOT"},
            "autofetch": {
                "interval": 1,
                "run_at_startup": 0,
                "sources": "",
                "config": "",
            },
        }
    )

    background_worker = auto_fetch.run_in_background(app, config)
    while not app.config["auto_fetch_event"]:
        pass
    interrupt = app.config["auto_fetch_event"]
    while background_worker.is_alive():
        if _state.called:
            interrupt.set_exit()
        else:
            interrupt.set()
    background_worker.join()

    _atexit.assert_called_once_with(interrupt.set_exit)
    _state.assert_called_once_with("FAKEROOT")


def test_header_auth_no_header():
    assert not auto_fetch.header_auth("NO", {}, b"")


def test_header_auth_token():
    assert auto_fetch.header_auth("NO_SECRET", {"Token": "NO_SECRET"}, b"")


def test_header_auth_gitlab():
    assert auto_fetch.header_auth("NO_SECRET", {"X-Gitlab-Token": "NO_SECRET"}, b"")


def test_header_auth_github():
    # create signature with: printf "{\"key\":\"payload\"}" | openssl sha256 -hmac NO_SECRET
    signature = (
        "sha256=3651f24b8b51336c4c76c957445514b56184580decff91ac369312eb2bc26ef6"
    )
    payload = b"""{"key":"payload"}"""
    assert auto_fetch.header_auth(
        "NO_SECRET", {"X-Hub-Signature-256": signature}, payload
    )


def test_header_auth_gitea():
    # create signature with: printf "{\"key\":\"payload\"}" | openssl sha256 -hmac NO_SECRET
    signature = (
        "3651f24b8b51336c4c76c957445514b56184580decff91ac369312eb2bc26ef6"
    )
    payload = b"""{"key":"payload"}"""
    assert auto_fetch.header_auth(
        "NO_SECRET", {"HTTP_X_GITEA_SIGNATURE": signature}, payload
    )