from unittest.mock import Mock, mock_open, patch, sentinel
from urllib import error, response

import pytest

from adop import exceptions, uploaders


def _make_urlopen_response(data, headers):
    fp = mock_open(read_data=data)()
    fp.closed = False
    return response.addinfo(fp, headers)


@patch("adop.uploaders.simple_api_uploader", spec=True)
def test_api_upload_calls_simple_ul(simple_ul):
    remote_data = {"url": "", "token": "", "insecure": 0}
    res = list(
        i
        for i in uploaders.api_upload(
            "./FAKEDIR/CACHE", "mylib", remote_data, {}, deploy=True
        )
    )
    assert res == []
    simple_ul.assert_called_once_with(
        "./FAKEDIR/CACHE", "mylib", remote_data, {}, deploy=True
    )


@patch("adop.uploaders.simple_api_uploader", spec=True, side_effect=exceptions.Fail)
def test_api_upload_calls_simple_ul_fail(simple_ul):
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in uploaders.api_upload(
                "./FAKEDIR/CACHE", "mylib", {}, {}, deploy=True
            )
        )
    simple_ul.assert_called_once()


@patch(
    "adop.uploaders.simple_api_uploader",
    spec=True,
    side_effect=error.HTTPError("", "", "", "", Mock()),
)
def test_api_upload_calls_simple_ul_HTTPError(simple_ul):
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in uploaders.api_upload(
                "./FAKEDIR/CACHE", "mylib", {}, {}, deploy=True
            )
        )
    simple_ul.assert_called_once()


@patch(
    "adop.uploaders.simple_api_uploader", spec=True, side_effect=error.URLError("", "")
)
def test_api_upload_calls_simple_ul_URLError(simple_ul):
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in uploaders.api_upload(
                "./FAKEDIR/CACHE", "mylib", {"url": ""}, {}, deploy=True
            )
        )
    simple_ul.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(
        b'//line1\n//line2\n//line3\n{"result_code":0}', {}
    ),
)
def test_simple_api_uploader(_urlopen):
    cache_path = Mock(**{"read_bytes.return_value": sentinel.UL_DATA})
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    res = list(
        i
        for i in uploaders.simple_api_uploader(
            cache_path, "mylib", remote_data, {}, deploy=True
        )
    )
    assert res
    assert "      line1" in res
    assert "      line2" in res
    assert "      line3" in res
    _urlopen.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(b'//line1\n{"result_code":1}', {}),
)
def test_simple_api_uploader_fail(_urlopen):
    cache_path = Mock(**{"read_bytes.return_value": sentinel.UL_DATA})
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in uploaders.simple_api_uploader(
                cache_path, "mylib", remote_data, {}, deploy=True
            )
        )
    _urlopen.assert_called_once()


@patch("adop.uploaders.simple_api_check_is_uploaded", spec=True)
def test_api_check_is_uploaded_calls_simple_fn(simple_fn):

    remote_data = {"url": "", "token": "", "insecure": 1}
    res = uploaders.api_check_is_uploaded("mylib", "AABBCC", "0.1.0", remote_data)
    assert res
    simple_fn.assert_called_once_with("mylib", "AABBCC", "0.1.0", remote_data)


@patch(
    "adop.uploaders.simple_api_check_is_uploaded",
    spec=True,
    side_effect=exceptions.Fail,
)
def test_api_check_is_uploaded_calls_simple_fn_fail(simple_fn):
    with pytest.raises(exceptions.CommandFail):
        uploaders.api_check_is_uploaded("", "", "", {})
    simple_fn.assert_called_once()


@patch(
    "adop.uploaders.simple_api_check_is_uploaded",
    spec=True,
    side_effect=error.HTTPError("", "", "", "", Mock()),
)
def test_api_check_is_uploaded_calls_simple_fn_HTTPError(simple_fn):
    with pytest.raises(exceptions.CommandFail):
        uploaders.api_check_is_uploaded("", "", "", {})
    simple_fn.assert_called_once()


@patch(
    "adop.uploaders.simple_api_check_is_uploaded",
    spec=True,
    side_effect=error.URLError("", ""),
)
def test_api_check_is_uploaded_calls_simple_fn_URLError(simple_fn):
    with pytest.raises(exceptions.CommandFail):
        uploaders.api_check_is_uploaded("", "", "", {"url": ""})
    simple_fn.assert_called_once()


@patch("adop.uploaders.simple_api_check_is_uploaded", spec=True, side_effect=KeyError)
def test_api_check_is_uploaded_calls_simple_fn_KeyError(simple_fn):
    with pytest.raises(exceptions.CommandFail):
        uploaders.api_check_is_uploaded("", "", "", {})
    simple_fn.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(b'{"mylib":{"AABBCC":{"tag":"0.1.0"}}}', {}),
)
def test_simple_api_check_is_uploaded(_urlopen):
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    res = uploaders.simple_api_check_is_uploaded(
        "mylib", "AABBCC", "0.1.0", remote_data
    )
    assert res
    _urlopen.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(b'{"mylib":{"AABBCC":{"tag":""}}}', {}),
)
def test_simple_api_check_is_uploaded_missing_tag(_urlopen):
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    res = uploaders.simple_api_check_is_uploaded(
        "mylib", "AABBCC", "0.1.0", remote_data
    )
    assert not res
    _urlopen.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(b'{"mylib":{}}', {}),
)
def test_simple_api_check_is_uploaded_missing_shasum(_urlopen):
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    res = uploaders.simple_api_check_is_uploaded(
        "mylib", "AABBCC", "0.1.0", remote_data
    )
    assert not res
    _urlopen.assert_called_once()


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(b"{}", {}),
)
def test_simple_api_check_is_uploaded_missing_root(_urlopen):
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 1}
    res = uploaders.simple_api_check_is_uploaded(
        "mylib", "AABBCC", "0.1.0", remote_data
    )
    assert not res
    _urlopen.assert_called_once()


@patch("adop.uploaders.simple_api_deploy", spec=True)
def test_api_deploy_calls_simple_deploy(simple_deploy):
    remote_data = {"url": "", "token": "", "insecure": 0}
    res = list(i for i in uploaders.api_deploy("mylib", remote_data, {}))
    assert res == []
    simple_deploy.assert_called_once_with("mylib", remote_data, {})


@patch(
    "urllib.request.urlopen",
    return_value=_make_urlopen_response(
        b'//line1\n//line2\n//line3\n{"result_code":0}', {}
    ),
)
def test_simple_api_deploy(_urlopen):
    remote_data = {"url": "uri://uri.local/", "token": "", "insecure": 0}
    res = list(i for i in uploaders.simple_api_deploy("mylib", remote_data, {}))
    assert res
    assert "      line1" in res
    assert "      line2" in res
    assert "      line3" in res
    _urlopen.assert_called_once()
