import configparser
import os
from unittest.mock import patch, sentinel

import pytest

from adop import exceptions, parse_config, zip_upload


def _cf(conf_string, conf_dict={}):
    cf = configparser.ConfigParser()
    cf.read_string(conf_string)
    cf.read_dict(conf_dict)
    return cf


class assert_startswith(str):
    def __eq__(self, other):
        return other.startswith(self)

    def __repr__(self):
        return "<assert_startswith>"


@patch(
    "adop.parse_config.parse",
    return_value=_cf(
        parse_config.default_config,
        {"remote:test": {"url": "", "token": "", "insecure": 0}},
    ),
)
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[requires]")
@patch.object(zip_upload.Uploader, "gen")
def test_upload_requires(upload_gen, _text, _exists, local_config):
    cf = local_config()
    remote_data = [{"url": "", "token": "", "insecure": False, "direct": False}]
    requires_data = {}
    install_data = {
        "install_section": "install:basedir",
        "install_root": os.path.expanduser(cf.get("install:basedir", "install_root")),
        "cache_root": os.path.expanduser(cf.get("install:basedir", "cache_root")),
    }

    def gen(*args):
        yield {"root": "mylib"}
        yield "comment 1"

    upload_gen.side_effect = gen

    zip_upload.upload(
        "requires.ini", "adop.ini", ".", ["test"], "basedir", False, False, False
    )

    upload_gen.assert_called_once_with(
        install_data, remote_data, requires_data, False, False, False
    )
    local_config.assert_called()
    _text.assert_called_once_with()
    _exists.assert_called_once_with()


@patch.object(zip_upload.Uploader, "gen")
def test_Uploader_upload(upload_gen, capfd):
    def gen(*args):
        yield {"root": "mylib"}
        yield "comment 1"
        yield {"result": "OK", "result_code": 0}

    upload_gen.side_effect = gen
    uploader = zip_upload.Uploader(
        _cf("", {"auto_delete": {"on": "1", "keep_on_disk": "0"}})
    )

    uploader.upload(
        sentinel.install, sentinel.remote, sentinel.require, False, False, False
    )

    upload_gen.assert_called_once_with(
        sentinel.install, sentinel.remote, sentinel.require, False, False, False
    )
    out = capfd.readouterr().out
    assert "Requires: mylib" in out
    assert "comment 1" in out
    assert "OK" in out


@patch.object(zip_upload.Uploader, "gen")
def test_Uploader_upload_commandfail(upload_gen, capfd):
    def gen(*args):
        yield {"root": "mylib"}
        raise exceptions.CommandFail("ERR!")

    upload_gen.side_effect = gen
    uploader = zip_upload.Uploader(
        _cf("", {"auto_delete": {"on": "1", "keep_on_disk": "0"}})
    )

    with pytest.raises(exceptions.CommandFail) as err:
        uploader.upload(
            sentinel.install, sentinel.remote, sentinel.require, False, False, False
        )
    assert "ERR!" in str(err.value)

    upload_gen.assert_called_once_with(
        sentinel.install, sentinel.remote, sentinel.require, False, False, False
    )
    assert "ERR" in capfd.readouterr().out


def test_Uploader_gen_no_requires():
    uploader = zip_upload.Uploader(_cf(""))
    res = list(
        i
        for i in uploader.gen(
            install_data={"cache_root": "./FAKEDIR/cache"},
            remote_data=[{}],
            requires_data={},
            deploy=False,
            force_deploy=False,
            force_upload=False,
        )
    )

    assert res == []


@patch(
    "adop.store_payload.find_file_from_headers",
    return_value=(sentinel.FILE, None, None),
)
@patch("adop.uploaders.api_check_is_uploaded", return_value=False)
@patch("adop.uploaders.api_upload")
@patch("adop.unpack_payload.extract_root_dir_name", return_value=sentinel.ZIP)
def test_Uploader_gen_requires_mylib(
    extract_root_dir_name, api_upload, api_check_is_uploaded, find_file_from_headers
):
    uploader = zip_upload.Uploader(_cf(""))
    res = list(
        i
        for i in uploader.gen(
            install_data={"cache_root": "./FAKEDIR/cache"},
            remote_data=[{}],
            requires_data={"mylib": "tag:0.0.1"},
            deploy=False,
            force_upload=False,
            force_deploy=False,
        )
    )

    def gen():
        yield {"root": "mylib"}
        yield assert_startswith("checking mylib=tag:")
        yield assert_startswith("using cache_root:")

    assert res == list(gen())
    api_upload.assert_called_once_with(
        sentinel.FILE,
        "mylib",
        {},
        {"Zip-Tag": "0.0.1", "Zip-Root": sentinel.ZIP},
        False,
    )
    find_file_from_headers.assert_called_once_with(
        "./FAKEDIR/cache", "mylib", {"Zip-Tag": "0.0.1", "Zip-Root": sentinel.ZIP}
    )


@patch(
    "adop.store_payload.find_file_from_headers",
    side_effect=exceptions.Fail("ERR"),
)
@patch("adop.uploaders.api_check_is_uploaded", return_value=False)
@patch("adop.uploaders.api_upload")
def test_Uploader_gen_requires_cache_not_found(
    api_upload, api_check_is_uploaded, find_file_from_headers
):
    uploader = zip_upload.Uploader(_cf(""))
    with pytest.raises(exceptions.CommandFail):
        list(
            i
            for i in uploader.gen(
                install_data={"cache_root": "./FAKEDIR/cache"},
                remote_data=[{}],
                requires_data={"mylib": "sha256:AABBCCDD"},
                deploy=True,
                force_upload=False,
                force_deploy=False,
            )
        )

    api_upload.assert_not_called()
    api_check_is_uploaded.assert_not_called()
    find_file_from_headers.assert_called_once()


@patch(
    "adop.store_payload.find_file_from_headers",
    return_value=(sentinel.FILE, None, None),
)
@patch("adop.uploaders.api_check_is_uploaded", return_value=True)
@patch("adop.uploaders.api_upload")
@patch("adop.unpack_payload.extract_root_dir_name", return_value=sentinel.ZIP)
def test_Uploader_gen_requires_mylib_already_uploaded(
    extract_root_dir_name, api_upload, api_check_is_uploaded, find_file_from_headers
):
    uploader = zip_upload.Uploader(_cf(""))
    res = list(
        i
        for i in uploader.gen(
            install_data={"cache_root": "./FAKEDIR/cache"},
            remote_data=[{"url": "URL"}],
            requires_data={"mylib": "tag:0.0.1"},
            deploy=False,
            force_upload=False,
            force_deploy=False,
        )
    )

    def gen():
        yield {"root": "mylib"}
        yield assert_startswith("checking mylib=tag:")
        yield assert_startswith("using cache_root:")
        yield assert_startswith("mylib already uploaded to")

    assert res == list(gen())
    api_upload.assert_not_called()
    find_file_from_headers.assert_called_once_with(
        "./FAKEDIR/cache", "mylib", {"Zip-Tag": "0.0.1", "Zip-Root": sentinel.ZIP}
    )


@patch(
    "adop.store_payload.find_file_from_headers",
    return_value=(sentinel.FILE, None, None),
)
@patch("adop.uploaders.api_check_is_uploaded", return_value=True)
@patch("adop.uploaders.api_upload")
@patch("adop.uploaders.api_deploy")
@patch("adop.unpack_payload.extract_root_dir_name", return_value=sentinel.ZIP)
def test_Uploader_gen_requires_mylib_force_deploy(
    extract_root_dir_name,
    api_deploy,
    api_upload,
    api_check_is_uploaded,
    find_file_from_headers,
):
    uploader = zip_upload.Uploader(_cf(""))
    res = list(
        i
        for i in uploader.gen(
            install_data={"cache_root": "./FAKEDIR/cache"},
            remote_data=[{"url": "URL"}],
            requires_data={"mylib": "tag:0.0.1"},
            deploy=False,
            force_upload=False,
            force_deploy=True,
        )
    )

    def gen():
        yield {"root": "mylib"}
        yield assert_startswith("checking mylib=tag:")
        yield assert_startswith("using cache_root:")
        yield assert_startswith("mylib already uploaded to")

    assert res == list(gen())
    api_upload.assert_not_called()
    api_deploy.assert_called_once_with(
        "mylib",
        {"url": "URL"},
        {"Zip-Tag": "0.0.1", "Zip-Root": sentinel.ZIP},
    )
    find_file_from_headers.assert_called_once_with(
        "./FAKEDIR/cache", "mylib", {"Zip-Tag": "0.0.1", "Zip-Root": sentinel.ZIP}
    )
