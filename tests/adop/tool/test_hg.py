import argparse
from unittest.mock import patch

from adop.tool import hg


@patch("adop.tool.hg.get_remote_from_hg")
@patch("argparse.ArgumentParser.parse_args")
def test_main_none(parse_args, get_remote_from_hg):
    parse_args.return_value = argparse.Namespace(get_remote=True)
    hg.main()
    parse_args.assert_called_once_with()
    get_remote_from_hg.assert_called_once()


@patch.dict("os.environ", {"HG_PATS": "[]"})
def test_get_destination_is_empty():
    assert "default" == hg.get_destination()


# TODO
# def test_get_destination_is_nothing():
#     os.environ["HG_PATS"] = ""
#     assert "default" == hg.get_destination()


@patch.dict("os.environ", clear=True)
def test_get_destination_env_not_defined():
    assert "default" == hg.get_destination()


@patch.dict("os.environ", {"HG_PATS": "['test']"})
def test_lowercase_get_destination_is_list():
    assert "test" == hg.get_destination()


@patch.dict("os.environ", {"HG_PATS": "['TEST']"})
def test_uppercase_get_destination_is_list():
    assert "TEST" == hg.get_destination()


@patch.dict("os.environ", {"HG_PATS": "['test1', 'test2']"})
def test_get_destination_is_list_multiple_items():
    assert "test1" == hg.get_destination()


@patch("pathlib.Path.is_file", return_value=True)
@patch("pathlib.Path.read_text", return_value="[sec1]\nkey1=val1")
def test_get_hgrc_lowercase(read_text, is_file):
    hgrc = hg.get_hgrc()
    assert "val1" == hgrc.get("sec1", "key1")
    is_file.assert_called_once()
    read_text.assert_called_once()


@patch("pathlib.Path.is_file", return_value=True)
@patch("pathlib.Path.read_text", return_value="[sec1]\nKey1=Val1")
def test_get_hgrc_uppercase(read_text, is_file):
    hgrc = hg.get_hgrc()
    assert "Val1" == hgrc.get("sec1", "Key1")
    is_file.assert_called_once()
    read_text.assert_called_once()


@patch("pathlib.Path.is_file", return_value=False)
@patch("pathlib.Path.read_text", return_value="")
def test_get_hgrc_no_file(read_text, is_file):
    hgrc = hg.get_hgrc()
    assert len(hgrc.items()) == 1
    is_file.assert_called_once()
    read_text.assert_not_called()


@patch.dict("os.environ", {"HG_PATS": "['ALIAS1']"})
@patch("pathlib.Path.is_file", return_value=True)
@patch("pathlib.Path.read_text", return_value="[paths]\nALIAS1=URL1")
def test_get_remote_from_hg_as_alias(read_text, is_file):

    res = hg.get_remote_from_hg()
    assert res == "ALIAS1"


@patch.dict("os.environ", {"HG_PATS": "['URL1']"})
@patch("pathlib.Path.is_file", return_value=True)
@patch("pathlib.Path.read_text", return_value="[paths]\ntest1=URL1")
def test_get_remote_from_hg_as_url(read_text, is_file):

    res = hg.get_remote_from_hg()
    assert res == "test1"


@patch.dict("os.environ", {"HG_PATS": "['srv4']"})
@patch("pathlib.Path.is_file", return_value=True)
@patch(
    "pathlib.Path.read_text",
    return_value="[paths]\nsrv4=URL1\n[adop:remotes]\nsrv4=remote4",
)
def test_lowercase_get_remote_from_hg_use_mapping(read_text, is_file):

    res = hg.get_remote_from_hg()
    assert res == "remote4"


@patch.dict("os.environ", {"HG_PATS": "['SRV44']"})
@patch("pathlib.Path.is_file", return_value=True)
@patch(
    "pathlib.Path.read_text",
    return_value="[paths]\nSRV44=srvurl1\n[adop:remotes]\nSRV44=REMOTE44",
)
def test_uppercase_get_remote_from_hg_use_mapping(read_text, is_file):

    res = hg.get_remote_from_hg()
    assert res == "REMOTE44"
