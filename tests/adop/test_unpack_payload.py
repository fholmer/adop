import io
import pathlib
import zipfile
from unittest.mock import Mock, mock_open, patch

import pytest

from adop import exceptions, unpack_payload


@pytest.fixture
def mock_zip_archive():
    def fn(file_list: str):
        zipdata = io.BytesIO()
        zipdata.close = Mock()
        zf = zipfile.ZipFile(zipdata, "w")
        for f in file_list:
            zf.writestr(f, "test")
        zf.close()
        return zipdata

    return fn


def update_mock_open(_open, _io_open, data):
    _open().__enter__ = Mock(return_value=data)
    _open().__exit__ = Mock(return_value=False)
    _open.reset_mock()
    _io_open.return_value = data


@patch("io.open", new_callable=Mock)
@patch("builtins.open", new_callable=mock_open)
def test_extract_root_dir_name_one_root(_open, _io_open, mock_zip_archive):
    update_mock_open(_open, _io_open, mock_zip_archive(["mylib/test.txt"]))
    filename = pathlib.Path.cwd().joinpath("FAKEFILE")

    root_dir = unpack_payload.extract_root_dir_name(filename)

    assert root_dir == "mylib"
    _open.assert_called_once()


@patch("io.open", new_callable=Mock)
@patch("builtins.open", new_callable=mock_open)
def test_extract_root_dir_name_multiple_roots(_open, _io_open, mock_zip_archive):
    update_mock_open(
        _open, _io_open, mock_zip_archive(["mylib1/test.txt", "mylib2/test.txt"])
    )
    filename = pathlib.Path.cwd().joinpath("FAKEFILE")

    with pytest.raises(exceptions.Fail):
        unpack_payload.extract_root_dir_name(filename)

    _open.assert_called_once()


@patch("io.open", new_callable=Mock)
@patch("builtins.open", new_callable=mock_open)
def test_extract_root_dir_name_one_root_and_file(_open, _io_open, mock_zip_archive):
    update_mock_open(_open, _io_open, mock_zip_archive(["mylib/test.txt", "test.txt"]))
    filename = pathlib.Path.cwd().joinpath("FAKEFILE")

    with pytest.raises(exceptions.Fail):
        unpack_payload.extract_root_dir_name(filename)

    _open.assert_called_once()


@patch("io.open", new_callable=Mock)
@patch("builtins.open", new_callable=mock_open)
def test_extract_root_dir_name_one_file(_open, _io_open, mock_zip_archive):
    update_mock_open(_open, _io_open, mock_zip_archive(["test.txt"]))
    filename = pathlib.Path.cwd().joinpath("FAKEFILE")

    with pytest.raises(exceptions.Fail):
        unpack_payload.extract_root_dir_name(filename)

    _open.assert_called_once()


def test_extract_root_dir_name_no_file():
    filename = pathlib.Path.cwd().joinpath("FAKEFILE")
    with pytest.raises(exceptions.Fail):
        unpack_payload.extract_root_dir_name(filename)


@patch("pathlib.Path.rmdir")
@patch("shutil.copyfileobj")
@patch("pathlib.Path.is_file")
@patch("pathlib.Path.glob")
@patch("pathlib.Path.unlink")
@patch("pathlib.Path.mkdir")
@patch("os.mkdir")
@patch("os.makedirs")
@patch("io.open", new_callable=Mock)
@patch("builtins.open", new_callable=mock_open)
def test_unpack(
    _open,
    _io_open,
    _os_mkdirs,
    _os_mkdir,
    _mkdir,
    _unlink,
    _glob,
    _isfile,
    _cpfileobj,
    _rmdir,
    mock_zip_archive,
):
    new_files = [f"mylib/test{i}.txt" for i in range(100)]
    update_mock_open(
        _open,
        _io_open,
        mock_zip_archive(new_files + ["mylib/mysubdir/"]),
    )

    filename = pathlib.Path("FAKEFILE")

    delete_files = [
        pathlib.Path("FAKEDIR", "mylib", f"untracked_test{i}.txt") for i in (range(102))
    ]
    _glob.return_value = delete_files

    res_list = []
    for res in unpack_payload.unpack(filename=filename, deploy_root="FAKEDIR"):
        res_list.append(res)

    assert len(res_list) > 0
    assert {"progress": 50} in res_list
    assert {"progress": 100} in res_list

    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_any_call(filename, "rb")
    _open.assert_any_call(str(pathlib.Path("FAKEDIR/mylib/test1.txt")), "wb")
    _open.assert_any_call(str(pathlib.Path("FAKEDIR/mylib/test2.txt")), "wb")
    _io_open.assert_called_with(str(filename), "rb")
    _os_mkdirs.assert_called_with(str(pathlib.Path("FAKEDIR/mylib")))
    _os_mkdir.assert_called_once_with(str(pathlib.Path("FAKEDIR/mylib/mysubdir")))
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _rmdir.assert_not_called()
    assert _unlink.call_count == len(delete_files)
    assert _isfile.call_count == len(delete_files)
    _glob.assert_called_once_with("**/*")
    _cpfileobj.assert_called()
    assert _cpfileobj.call_count == len(new_files)
