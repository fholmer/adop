import pathlib
from unittest.mock import mock_open, patch

import pytest

from adop import auto_fetch_state


def test_state_make():
    # sha256 sum
    assert (
        auto_fetch_state.State.make(b"00")
        == "f1534392279bddbf9d43dde8701cb5be14b82f76ec6607bf8d6ad557f60f304e"
    )

    # TypeError if not a bytestring

    with pytest.raises(TypeError):
        auto_fetch_state.State.make("")

    with pytest.raises(TypeError):
        auto_fetch_state.State.make(0)

    with pytest.raises(TypeError):
        auto_fetch_state.State.make(None)


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text")
def test_state_get(_text, _mkdir, _exists):
    _exists.return_value = True
    _text.return_value = "[myroot]\nlast_update=0011"
    config_file_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    state = auto_fetch_state.State(str(config_file_path))

    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _text.assert_called_once_with()
    _exists.assert_called_once_with()

    assert state.config.sections() == ["myroot"]
    assert state.get("myroot") == "0011"
    assert state.get("section_is_missing") is None


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
def test_state_update(_mkdir, _exists):
    _exists.return_value = False
    config_file_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    state = auto_fetch_state.State(str(config_file_path))
    state.update("newlib", "2233")

    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _exists.assert_called_once_with()

    assert state.config.sections() == ["newlib"]
    assert state.get("newlib") == "2233"


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_state_store(_open, _mkdir, _exists):
    _exists.return_value = False
    config_file_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    state = auto_fetch_state.State(str(config_file_path))
    state.update("testlib", "4455")
    state.store()

    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _exists.assert_called_once_with()
    _open.assert_called_once_with(mode="w")
    handle = _open()
    handle.write.assert_called()
