import configparser
import pathlib
from unittest.mock import mock_open, patch

import pytest

from adop import exceptions, parse_config


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text")
def test_parse_override_pass(_text, _mkdir, _exists):
    _exists.return_value = True
    _text.return_value = """
        [server]
        host=0.0.0.0
        port=80
        [auth]
        token=NO"""
    config_file_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    config = parse_config.parse(config_file=config_file_path, host=None, port=None)

    assert config["server"].get("host") == "0.0.0.0"
    assert config["server"].get("port") == "80"
    _text.assert_called_once_with()
    _mkdir.assert_not_called()


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text")
def test_parse_override_host_port(_text, _mkdir, _exists):
    _exists.return_value = True
    _text.return_value = """
        [server]
        host=0.0.0.0
        port=80
        [auth]
        token=NO"""
    config_file_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    config = parse_config.parse(
        config_file=config_file_path, host="127.0.0.1", port=8000
    )
    assert config["server"].get("host") == "127.0.0.1"
    assert config["server"].get("port") == "8000"

    _text.assert_called_once_with()
    _mkdir.assert_not_called()


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_parse_not_exists(_open, _mkdir, _exists):
    _exists.return_value = False
    config_file_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    config = parse_config.parse(
        config_file=config_file_path, host="127.0.0.1", port=8000
    )

    assert len(config["server"].get("write_token")) >= 32
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with(mode="w")
    handle = _open()
    handle.write.assert_called()


@patch("pathlib.Path.exists")
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.read_text")
def test_parse_interpolation(_text, _mkdir, _exists):
    _exists.return_value = True
    _text.return_value = """
        [server]
        host=127.0.0.1
        port=80
        [auth]
        token=NO
        [remote:local]
        url = http://${server:host}:${server:port}/api/v1
        token = ${auth:token}
        """
    config_file_path = pathlib.Path.cwd().joinpath("FAKEDIR")

    config = parse_config.parse(config_file=config_file_path, host=None, port=None)

    assert config["remote:local"].get("url") == "http://127.0.0.1:80/api/v1"
    assert config["remote:local"].get("token") == "NO"
    _text.assert_called_once_with()
    _mkdir.assert_not_called()


@patch("adop.hooks.getenv", return_value="TEST123")
def test_get_env_interpolation(_getenv):
    get_env = parse_config.GetEnvInterpolation()
    parser = configparser.ConfigParser(interpolation=get_env)
    parser.set("DEFAULT", "test", "${hook:env:TEST_TOKEN}")
    assert parser.get("DEFAULT", "test") == "TEST123"
    _getenv.assert_called_once_with("TEST_TOKEN")


@patch("adop.hooks.getkeyring", return_value="TEST123")
def test_get_keyring_interpolation(_getenv):
    get_env = parse_config.GetEnvInterpolation()
    parser = configparser.ConfigParser(interpolation=get_env)
    parser.set("DEFAULT", "test", "${hook:keyring:SERVICE:USER}")
    assert parser.get("DEFAULT", "test") == "TEST123"
    _getenv.assert_called_once_with("SERVICE:USER")


@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[test1]\ntest2=test3")
def test_config_get(_exists, _read_text, capfd):
    parse_config.config_get("test1", "test2", "./adop.ini", "", False)
    assert "test3" in capfd.readouterr().out


@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[test1]\ntest2=test3")
def test_config_get_empty(_exists, _read_text, capfd):
    with pytest.raises(exceptions.CommandFail):
        parse_config.config_get("test", "test", "./adop.ini", "", False)
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_init(_open, _mkdir, _exists):
    parse_config.config_init("./FAKEDIR/.adop.ini", "", False)
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with(mode="w")
    handle = _open()
    handle.write.assert_any_call("[server]\n")
    handle.write.assert_any_call("on = 1\n")


@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_init_merge(_open, _mkdir, _exists):
    parse_config.config_init("./FAKEDIR/.adop.ini", "", True)
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_with(mode="w")
    handle = _open()
    handle.write.assert_any_call("[server]\n")
    handle.write.assert_any_call("on = 1\n")


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_set(_open, _mkdir, _exists, capfd):
    parse_config.config_set("test1", "test2", "test3", "./FAKEDIR/.adop.ini", "")
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with("w")
    handle = _open()
    handle.write.assert_any_call("[test1]\n")
    handle.write.assert_any_call("test2 = test3\n")
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.read_text", return_value="[test1]\ntest2=test3")
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_set_no_write_already_exists(_open, _mkdir, _exists, _read_text, capfd):
    parse_config.config_set("test1", "test2", "test3", "./FAKEDIR/.adop.ini", "")
    _mkdir.assert_not_called()
    _open.assert_not_called()
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.read_text", return_value="[test1]\ntest2=test3\n")
def test_parse_config_list(_text, _exists, capfd):
    parse_config.config_list("./FAKEDIR/.adop.ini", "", False)
    out = capfd.readouterr().out
    assert "[test1]" in out
    assert "test2 = test3" in out


@patch("pathlib.Path.exists", return_value=False)
def test_parse_config_list_file_not_found(_exists):
    with pytest.raises(exceptions.CommandFail):
        parse_config.config_list("./FAKEDIR/.adop.ini", "", False)


def test_generate_token(capfd):
    parse_config.generate_token()
    assert len(capfd.readouterr().out) >= 32


@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.mkdir")
@patch(
    "pathlib.Path.read_text",
    side_effect=["[test1]\ntest2=test3\n", "[test1]\ntest2=test4\n"],
)
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_import_file(_open, _read_text, _mkdir, _exists, capfd):
    CONFIG_FILE = "./FAKEDIR/.adop.ini"
    IMPORT_FILE = ".import.ini"
    parse_config.config_import(CONFIG_FILE, "", IMPORT_FILE)
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with("w")
    handle = _open()
    handle.write.assert_any_call("[test1]\n")
    handle.write.assert_any_call("test2 = test4\n")
    assert f"Imported {IMPORT_FILE} into {CONFIG_FILE}" in capfd.readouterr().out


@patch(
    "pathlib.Path.read_text",
    return_value=(
        "[remote:myremote]\n" "url=URL\n" "insecure=0\n" "token=${hook:env:PASS}\n"
    ),
)
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_add_remote_no_write(_open, _mkdir, _exists, _read_text, capfd):
    parse_config.config_add_remote(
        "myremote", "URL", False, "", "PASS", "", "./FAKEDIR/.adop.ini", ""
    )
    _mkdir.assert_not_called()
    _open.assert_not_called()
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
@patch("getpass.getpass", return_value="PASS")
def test_config_add_remote_pass(_getpass, _open, _mkdir, _exists, capfd):
    parse_config.config_add_remote(
        "myremote", "URL", False, "PASS", "", "", "./FAKEDIR/.adop.ini", ""
    )
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with("w")
    _getpass.assert_called_once_with()
    handle = _open()
    handle.write.assert_any_call("[remote:myremote]\n")
    handle.write.assert_any_call("url = URL\n")
    handle.write.assert_any_call("insecure = 0\n")
    handle.write.assert_any_call("token = PASS\n")
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_add_remote_env(_open, _mkdir, _exists, capfd):
    parse_config.config_add_remote(
        "myremote", "URL", False, "", "PASS", "", "./FAKEDIR/.adop.ini", ""
    )
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with("w")
    handle = _open()
    handle.write.assert_any_call("[remote:myremote]\n")
    handle.write.assert_any_call("url = URL\n")
    handle.write.assert_any_call("insecure = 0\n")
    handle.write.assert_any_call("token = ${hook:env:PASS}\n")
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_add_remote_keyring(_open, _mkdir, _exists, capfd):
    parse_config.config_add_remote(
        "myremote", "URL", False, "", "", "SERVICE:USER", "./FAKEDIR/.adop.ini", ""
    )
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with("w")
    handle = _open()
    handle.write.assert_any_call("[remote:myremote]\n")
    handle.write.assert_any_call("url = URL\n")
    handle.write.assert_any_call("insecure = 0\n")
    handle.write.assert_any_call("token = ${hook:keyring:SERVICE:USER}\n")
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_add_install(_open, _mkdir, _exists, capfd):
    parse_config.config_add_install(
        "myinstall", "./FAKEDIR/ROOT", "./FAKEDIR/CACHE", "./FAKEDIR/.adop.ini", ""
    )
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with("w")
    handle = _open()
    handle.write.assert_any_call("[install:myinstall]\n")
    handle.write.assert_any_call("install_root = ./FAKEDIR/ROOT\n")
    handle.write.assert_any_call("cache_root = ./FAKEDIR/CACHE\n")
    assert capfd.readouterr().out == ""


@patch(
    "pathlib.Path.read_text",
    return_value=(
        "[install:myinstall]\n"
        "install_root=./FAKEDIR/ROOT\n"
        "cache_root=./FAKEDIR/CACHE\n"
    ),
)
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_add_install_no_write(_open, _mkdir, _exists, _read_text, capfd):
    parse_config.config_add_install(
        "myinstall", "./FAKEDIR/ROOT", "./FAKEDIR/CACHE", "./FAKEDIR/.adop.ini", ""
    )
    _mkdir.assert_not_called()
    _open.assert_not_called()
    assert capfd.readouterr().out == ""


@patch("pathlib.Path.exists", return_value=False)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_add_autofetch_source(_open, _mkdir, _exists, capfd):

    parse_config.config_add_autofetch_source(
        "mysource",
        "MYROOT",
        "ZIPROOT",
        "CHECK",
        "PAYLOAD",
        "HEADERS",
        "./FAKEDIR/.adop.ini",
        "",
    )
    _mkdir.assert_called_once_with(parents=True, exist_ok=True)
    _open.assert_called_once_with("w")
    handle = _open()
    handle.write.assert_any_call("[autofetch:mysource]\n")
    handle.write.assert_any_call("root = MYROOT\n")
    handle.write.assert_any_call("zip_root = ZIPROOT\n")
    handle.write.assert_any_call("check_url = CHECK\n")
    handle.write.assert_any_call("payload_url = PAYLOAD\n")
    handle.write.assert_any_call("headers = HEADERS\n")
    assert capfd.readouterr().out == ""


@patch(
    "pathlib.Path.read_text",
    return_value=(
        "[autofetch:mysource]\n"
        "root = MYROOT\n"
        "zip_root=ZIPROOT\n"
        "check_url=CHECK\n"
        "payload_url=PAYLOAD\n"
        "headers=HEADERS\n"
    ),
)
@patch("pathlib.Path.exists", return_value=True)
@patch("pathlib.Path.mkdir")
@patch("pathlib.Path.open", new_callable=mock_open)
def test_config_add_autofetch_source_no_write(
    _open, _mkdir, _exists, _read_text, capfd
):

    parse_config.config_add_autofetch_source(
        "mysource",
        "MYROOT",
        "ZIPROOT",
        "CHECK",
        "PAYLOAD",
        "HEADERS",
        "./FAKEDIR/.adop.ini",
        "",
    )
    _mkdir.assert_not_called()
    _open.assert_not_called()
    assert capfd.readouterr().out == ""
